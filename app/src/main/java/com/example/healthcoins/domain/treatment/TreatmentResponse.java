package com.example.healthcoins.domain.treatment;

import android.support.annotation.Nullable;

import com.example.healthcoins.domain.Resource;

public class TreatmentResponse extends Resource<TreatmentDetail> {

    public TreatmentResponse(@Nullable TreatmentDetail data, @Nullable String message, @Nullable String code) {
        super(data, message, code);
    }
}
