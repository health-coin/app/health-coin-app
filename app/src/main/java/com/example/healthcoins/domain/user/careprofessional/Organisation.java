package com.example.healthcoins.domain.user.careprofessional;

public class Organisation {
    private String _id;
    private Location[] locations;
    private String name;

    public Organisation(String id, Location[] locations, String name) {
        _id = id;
        this.locations = locations;
        this.name = name;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location[] getLocations() {
        return locations;
    }

    public Location getFirstLocation() {
        return locations[0];
    }

    public void setLocations(Location[] locations) {
        this.locations = locations;
    }
}
