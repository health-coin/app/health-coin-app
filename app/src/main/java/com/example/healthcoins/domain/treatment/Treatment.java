package com.example.healthcoins.domain.treatment;

import com.example.healthcoins.domain.treatment.measurement.Measurement;

/**
 * Off-chain data.
 */
public class Treatment {
    private String _id;
    private String participantId;
    private String careProviderId;
    private String contractAddress;
    private Measurement[] customMeasurements;

    Treatment(
            String _id,
            String participantId,
            String careProviderId,
            String contractAddress,
            Measurement[] customMeasurements
    ) {
        this._id = _id;
        this.participantId = participantId;
        this.careProviderId = careProviderId;
        this.contractAddress = contractAddress;
        this.customMeasurements = customMeasurements;
    }
    public String getId() {
        return _id;
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getCareProviderId() {
        return careProviderId;
    }

    public void setCareProviderId(String careProviderId) {
        this.careProviderId = careProviderId;
    }

    public String getContractAddress() {
        return contractAddress;
    }

    public void setContractAddress(String contractAddress) {
        this.contractAddress = contractAddress;
    }

    public Measurement[] getCustomMeasurements() {
        return customMeasurements;
    }

    public void setCustomMeasurements(Measurement[] customMeasurements) {
        this.customMeasurements = customMeasurements;
    }
}
