package com.example.healthcoins.domain.user.careprofessional;

import com.example.healthcoins.domain.user.PersonData;

public class CareProfessional extends PersonData {

    private String registeredAt;
    private String role;
    private String email;
    private Organisation organisation;
    private String speciality;

    public CareProfessional(String id, String dateOfBirth, String firstName, String lastName, String policyNumber, String phoneNumber, String gender, String address, String registeredAt, String role, String email, Organisation organisation, String speciality) {
        super(id, dateOfBirth, firstName, lastName, policyNumber, phoneNumber, gender, address);
        this.registeredAt = registeredAt;
        this.role = role;
        this.email = email;
        this.organisation = organisation;
        this.speciality = speciality;
    }

    public String getRegisteredAt() {
        return registeredAt;
    }

    public void setRegisteredAt(String registeredAt) {
        this.registeredAt = registeredAt;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Organisation getOrganisation() {
        return organisation;
    }

    public void setOrganisation(Organisation organisation) {
        this.organisation = organisation;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getFullAddress() {
        if(this.organisation.getLocations().length > 0) {
            LocationAddress firstLocation = this.organisation.getLocations()[0].getAddress();
            return firstLocation.getStreet() + " " + firstLocation.getStreetNumber() + " " + firstLocation.getPostalCode() + " " + firstLocation.getCity();
        }
        return "Geen adres bekend";
    }
}
