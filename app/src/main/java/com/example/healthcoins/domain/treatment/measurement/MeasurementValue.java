package com.example.healthcoins.domain.treatment.measurement;


import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.widget.EditText;

import com.example.healthcoins.BR;

import java.io.Serializable;
import java.util.Date;

public class MeasurementValue extends BaseObservable implements Serializable {
    private String id;
    private String unit;
    private Integer quantity;
    private String comparisonText;
    private Date date;

    public MeasurementValue(String id, String unit, Integer quantity) {
        this.id = id;
        this.unit = unit;
        this.quantity = quantity;
        this.setComparisonText("<");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    @Bindable
    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        if(!this.unit.equals(unit)) {
            this.unit = unit;
            this.notifyPropertyChanged(BR.unit);
        }
    }

    @Bindable
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        if(!this.quantity.equals(quantity)) {
            this.quantity = quantity;
            this.notifyPropertyChanged(BR.quantity);
        }
    }

    public String getComparisonText() {
        return comparisonText;
    }

    public void setComparisonText(String comparisonText) {
        this.comparisonText = comparisonText;
    }

    /**
     * For displaying measurement in a simple listview.
     * @return pretty display
     */
    @Override
    public String toString() {
        return this.getQuantity() + " " + this.getUnit();
    }

    @BindingAdapter("quantityValidator")
    public static void quantityValidator(EditText editText, String quantity) {
        if (TextUtils.isEmpty(editText.getText().toString())) {
            editText.setError(null);
        } else if (editText.getText().toString().length() <= 1) {
            editText.setError("Waarde moet twee nummers lang zijn.");
        } else if (editText.getText().toString().length() > 3) {
            editText.setError("Waarde mag niet langer dan 4 nummers zijn");
        } else editText.setError(null);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof MeasurementValue) {
            MeasurementValue newValue = (MeasurementValue) obj;
            return newValue.getUnit().equals(this.getUnit()) && newValue.getQuantity() <= this.getQuantity();
        }
        return false;
    }
}
