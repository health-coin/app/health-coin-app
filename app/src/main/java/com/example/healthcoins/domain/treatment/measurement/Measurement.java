package com.example.healthcoins.domain.treatment.measurement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Measurement implements Serializable {

    private Integer id;
    private Date date;
    private boolean doneByProfessional = false;
    private List<MeasurementValue> values;

    public Measurement() {
        this.values = new ArrayList<>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isDoneByProfessional() {
        return doneByProfessional;
    }

    public void setDoneByProfessional(boolean doneByProfessional) {
        this.doneByProfessional = doneByProfessional;
    }

    public void addValue(MeasurementValue value) {
        values.add(value);
    }

    public void removeValue(MeasurementValue value) {
        values.remove(value);
    }

    public void setValues(List<MeasurementValue> values) {
        this.values = values;
    }

    public MeasurementValue findMeasurementValueById(Integer id) {
        for (MeasurementValue measurementValue : values) {
            if(measurementValue.getId().equals(id)) {
                return measurementValue;
            }
        }
        return null;
    }

    public List<MeasurementValue> getValues() {
        return values;
    }

    public boolean hasMeasurements() {
        return getValues() != null && getValues().size() > 0;
    }
}
