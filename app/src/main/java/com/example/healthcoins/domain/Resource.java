package com.example.healthcoins.domain;/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.support.annotation.Nullable;



/**
 * A generic class that holds a value with its loading status.
 * @param <T>
 */
public class Resource<T> {

    @Nullable
    public final String message;

    @Nullable
    public final T data;

    @Nullable
    public final String code;

    public Resource(@Nullable T data, @Nullable String message, @Nullable String code) {
        this.data = data;
        this.message = message;
        this.code = code;
    }

    @Override
    public String toString() {
        return "Resource{" +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}