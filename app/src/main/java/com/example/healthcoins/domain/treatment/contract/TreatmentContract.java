package com.example.healthcoins.domain.treatment.contract;

import com.example.healthcoins.domain.treatment.TreatmentState;

/**
 * On chain data.
 */
public class TreatmentContract {

    private String careTaker;
    private String participant;
    private Long plannedEndDate;
    private Long completionDate;
    private Long startDate;
    private Integer coinReward;
    private String state;
    private boolean redeemed;

    TreatmentContract(
            String careTaker,
            String participant,
            Long plannedEndDate,
            Long completionDate,
            Long startDate,
            Integer coinReward,
            String state,
            boolean redeemed
    ) {
        this.careTaker = careTaker;
        this.participant = participant;
        this.plannedEndDate = plannedEndDate;
        this.completionDate = completionDate;
        this.startDate = startDate;
        this.coinReward = coinReward;
        this.state = state;
        this.redeemed = redeemed;
    }

    public String getCareTaker() {
        return careTaker;
    }

    public void setCareTaker(String careTaker) {
        this.careTaker = careTaker;
    }

    public String getParticipant() {
        return participant;
    }

    public void setParticipant(String participant) {
        this.participant = participant;
    }

    public Long getPlannedEndDate() {
        return plannedEndDate;
    }

    public void setPlannedEndDate(Long plannedEndDate) {
        this.plannedEndDate = plannedEndDate;
    }

    public Long getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Long completionDate) {
        this.completionDate = completionDate;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Integer getCoinReward() {
        return coinReward;
    }

    public void setCoinReward(Integer coinReward) {
        this.coinReward = coinReward;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isRedeemed() {
        return redeemed;
    }

    public void setRedeemed(boolean redeemed) {
        this.redeemed = redeemed;
    }

    public boolean isPending() {
        return TreatmentState.valueOf(getState()) == TreatmentState.PENDING;
    }

    public boolean isStarted() {
        return TreatmentState.valueOf(getState()) == TreatmentState.STARTED;
    }

    public boolean isEndMeasurementAdded() {
        return TreatmentState.valueOf(getState()) == TreatmentState.END_MEASUREMENT_ADDED;

    }
    public boolean isDone() {
        return TreatmentState.valueOf(getState()) == TreatmentState.DONE;
    }

    public boolean isSigned() {
        return TreatmentState.valueOf(getState()) == TreatmentState.SIGNED;
    }
}
