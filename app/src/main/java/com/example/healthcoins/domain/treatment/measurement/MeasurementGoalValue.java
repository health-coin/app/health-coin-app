package com.example.healthcoins.domain.treatment.measurement;

public class MeasurementGoalValue extends MeasurementValue {

    private boolean helpUI;
    private String helpText;

    public MeasurementGoalValue(String id, String unit, Integer quantity) {
        super(id, unit, quantity);
    }

    public String getHelpText() {
        return helpText;
    }

    public void setHelpText(String helpText) {
        this.helpText = helpText;
    }

    public boolean isHelpUI() {
        return helpUI;
    }

    public void setHelpUI(boolean helpUI) {
        this.helpUI = helpUI;
    }

}
