package com.example.healthcoins.domain.user.careprofessional;

public class LocationAddress {
    private String _id;
    private String country;
    private String addition;
    private String city;
    private String postalCode;
    private String street;
    private Integer streetNumber;

    public LocationAddress(String id, String country, String addition, String city, String postalCode, String street, Integer streetNumber) {
        _id = id;
        this.country = country;
        this.addition = addition;
        this.city = city;
        this.postalCode = postalCode;
        this.street = street;
        this.streetNumber = streetNumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getAddition() {
        return addition;
    }

    public void setAddition(String addition) {
        this.addition = addition;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getFormattedPostalCode() {
        return getPostalCode().substring(0, 4) + " " + getPostalCode().substring(4);
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(Integer streetNumber) {
        this.streetNumber = streetNumber;
    }
}
