package com.example.healthcoins.domain.treatment;

public enum TreatmentState {
    DONE,
    STARTED,
    PENDING,
    SIGNED,
    END_MEASUREMENT_ADDED
}
