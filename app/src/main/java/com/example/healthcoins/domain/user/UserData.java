package com.example.healthcoins.domain.user;

import com.example.healthcoins.domain.auth.TokenData;

public class UserData {

    private String address;
    private String registeredAt;
    private String role;
    private String email;
    private TokenData token;

    public UserData(String address, String registeredAt, String role, String email) {
        this.address = address;
        this.registeredAt = registeredAt;
        this.role = role;
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public String getRegisteredAt() {
        return registeredAt;
    }

    public String getRole() {
        return role;
    }

    public String getEmail() {
        return email;
    }

    public TokenData getToken() {
        return token;
    }

    public void setToken(TokenData token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof UserData) {
            UserData compareTo = (UserData) obj;
            return compareTo.getAddress().equals(this.address);
        } else {
            return false;
        }
    }
}
