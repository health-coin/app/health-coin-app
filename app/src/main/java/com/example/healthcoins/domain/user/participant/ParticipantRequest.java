package com.example.healthcoins.domain.user.participant;

import com.example.healthcoins.domain.user.PersonData;

public class ParticipantRequest {

    private String email;
    private String password;
    private Integer personalIdNumber;
    private PersonData personProperties;

    public ParticipantRequest(String email, String password, Integer personalIdNumber, PersonData personProperties) {
        this.email = email;
        this.password = password;
        this.personalIdNumber = personalIdNumber;
        this.personProperties = personProperties;
    }

}
