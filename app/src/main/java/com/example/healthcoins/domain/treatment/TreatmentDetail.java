package com.example.healthcoins.domain.treatment;

import com.example.healthcoins.domain.treatment.contract.TreatmentContract;
import com.example.healthcoins.domain.treatment.measurement.Measurement;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;

import java.io.Serializable;
import java.util.List;

/**
 * Includes Contract measurement values.
 */
public class TreatmentDetail implements Serializable {

    private TreatmentContract contractData;
    private Treatment treatmentData;
    private MeasurementValue[] goalMeasurement;
    private Measurement[] customMeasurements;
    private MeasurementValue[] startMeasurement;
    private MeasurementValue[] endMeasurement;
    private Integer pendingCoins;
    private Integer finalCoins;
    private String statusText;

    TreatmentDetail(TreatmentContract contractData) {
        this.contractData = contractData;
    }

    public TreatmentContract getContractData() {
        return contractData;
    }

    public void setContractData(TreatmentContract contractData) {
        this.contractData = contractData;
    }

    public Treatment getTreatmentData() {
        return treatmentData;
    }

    public void setTreatmentData(Treatment treatmentData) {
        this.treatmentData = treatmentData;
    }

    public MeasurementValue[] getGoalMeasurement() {
        return goalMeasurement;
    }

    public void setGoalMeasurement(MeasurementValue[] goalMeasurement) {
        this.goalMeasurement = goalMeasurement;
    }

    public Measurement[] getCustomMeasurements() {
        return customMeasurements;
    }

    public void setCustomMeasurements(Measurement[] customMeasurements) {
        this.customMeasurements = customMeasurements;
    }

    public MeasurementValue[] getStartMeasurement() {
        return startMeasurement;
    }

    public void setStartMeasurement(MeasurementValue[] startMeasurement) {
        this.startMeasurement = startMeasurement;
    }

    public MeasurementValue[] getEndMeasurement() {
        return endMeasurement;
    }

    public void setEndMeasurement(MeasurementValue[] endMeasurement) {
        this.endMeasurement = endMeasurement;
    }

    public Integer getPendingCoins() {
        return pendingCoins;
    }

    public void setPendingCoins(Integer pendingCoins) {
        this.pendingCoins = pendingCoins;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public boolean hasStartMeasurement() {
        return hasMeasurement(startMeasurement);
    }

    public boolean hasCustomMeasurement() {
        return hasMeasurement(customMeasurements);
    }

    public boolean hasEndMeasurement() {
        return hasMeasurement(endMeasurement);
    }

    private boolean hasMeasurement(Object[] measurements) {
        return measurements != null && measurements.length > 0;
    }

    public Integer getFinalCoins() {
        return finalCoins;
    }

    public void setFinalCoins(Integer finalCoins) {
        this.finalCoins = finalCoins;
    }
}
