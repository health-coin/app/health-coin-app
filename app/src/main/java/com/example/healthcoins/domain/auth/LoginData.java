package com.example.healthcoins.domain.auth;

import com.example.healthcoins.domain.user.UserData;

public class LoginData extends UserData {

    private final TokenData token;

    public LoginData(String address, String registeredAt, String role, String email, TokenData token) {
        super(address, registeredAt, role, email);
        this.token = token;
    }

    public TokenData getToken() {
        return token;
    }
}
