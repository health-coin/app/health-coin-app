package com.example.healthcoins.domain.auth;

public class TokenData {

    private String accessToken;
    private String refreshToken;
    private String expirationDate;

    public TokenData(String accessToken, String refreshToken, String expirationDate) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.expirationDate = expirationDate;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    @Override
    public String toString() {
        return "Token: " + getAccessToken() + " ExpirationDate: " + getExpirationDate();
    }
}
