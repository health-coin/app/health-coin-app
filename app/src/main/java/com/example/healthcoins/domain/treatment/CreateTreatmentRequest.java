package com.example.healthcoins.domain.treatment;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.healthcoins.BR;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;

public class CreateTreatmentRequest extends BaseObservable {
    private String coinReward;
    private String endDate;
    private String participantId;
    private MeasurementValue[] measurements;

    public CreateTreatmentRequest(String coinReward, String endDate, String participantId, MeasurementValue[] measurements) {
        this.coinReward = coinReward;
        this.endDate = endDate;
        this.participantId = participantId;
        this.measurements = measurements;
    }
    public CreateTreatmentRequest() {
    }

    @Bindable
    public String getCoinReward() {
        return coinReward;
    }

    public void setCoinReward(String coinReward) {
        this.coinReward = coinReward;
        notifyPropertyChanged(BR.coinReward);
    }

    @Bindable
    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
        notifyPropertyChanged(BR.endDate);

    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public MeasurementValue[] getMeasurements() {
        return measurements;
    }

    public void setMeasurements(MeasurementValue[] measurements) {
        this.measurements = measurements;
    }
}
