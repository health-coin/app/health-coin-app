package com.example.healthcoins.domain.transaction;

public class BalanceData {

    private int balance;

    public BalanceData(int balance) {
        this.balance = balance;
    }

    public int getBalance() {
        return balance;
    }
}
