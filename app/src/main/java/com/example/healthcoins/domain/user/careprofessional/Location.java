package com.example.healthcoins.domain.user.careprofessional;

public class Location {
    private String _id;
    private LocationAddress address;
    private String name;

    public Location(String id, LocationAddress address, String name) {
        _id = id;
        this.address = address;
        this.name = name;
    }

    public LocationAddress getAddress() {
        return address;
    }

    public void setAddress(LocationAddress address) {
        this.address = address;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
