package com.example.healthcoins.domain.user;

public class PersonData {

    private String _id;
    private String dateOfBirth;
    private String firstName;
    private String lastName;
    private String policyNumber;
    private String phoneNumber;
    private String gender;
    private String address; // note: blockchain sender address!

    public PersonData(String id, String dateOfBirth, String firstName, String lastName, String policyNumber, String phoneNumber, String gender, String address) {
        _id = id;
        this.dateOfBirth = dateOfBirth;
        this.firstName = firstName;
        this.lastName = lastName;
        this.policyNumber = policyNumber;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
        this.address = address;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getFullName () {
        return this.firstName + " " + this.lastName;
    }

    public String getGenderDisplayable() {
        switch(this.getGender()) {
            case "1":
                return "Man";
            case "2":
                return "Vrouw";
            default:
                return "Anders";
        }
    }

    public String getGenderSolutation() {
        switch(this.getGender()) {
            case "0":
                return "Dhr.";
            case "1":
                return "Mvr.";
            default:
                return "";
        }
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }


}
