package com.example.healthcoins.domain;/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A generic class that can provide a resource backed by both the sqlite database and the network.
 * <p>
 * You can read more about it in the <a href="https://developer.android.com/arch">Architecture
 * Guide</a>.
 * @param <ResultType>
 * @param <RequestType>
 */
public abstract class NetworkBoundResource<ResultType, RequestType> {

    private final MediatorLiveData<Resource<ResultType>> result = new MediatorLiveData<>();

    @MainThread
    public NetworkBoundResource() {
        fetchFromNetwork();

    }

    @MainThread
    private void setValue(Resource<ResultType> newValue) {
        if (!Objects.equals(result.getValue(), newValue)) {
            result.setValue(newValue);
        }
    }

    private void fetchFromNetwork() {

        Call<ApiResponse<RequestType>> apiResponse = createCall();
        // we re-attach dbSource as a new source, it will dispatch its latest value quickly
        apiResponse.enqueue(new Callback<ApiResponse<RequestType>>() {
            @Override
            public void onResponse(Call<ApiResponse<RequestType>> call, Response<ApiResponse<RequestType>> response) {
                if (response.isSuccessful()) {
                    Resource resource = new Resource(response.body().data, response.body().message, null);
                    setValue(resource);

                } else {
                    onFetchFailed();
                    try {
                        Resource resource = getErrorMessage(response.errorBody().string());
                        setValue(resource);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<RequestType>> apiResponseCall, Throwable error) {
                Resource resource = new Resource(null, error.getMessage(), null);
                setValue(resource);
                Log.d("Network error", error.getMessage());
            }
        });
    }

    private Resource getErrorMessage (String errorBody) {
        Gson gson = new Gson();
        Resource errorResponse = gson.fromJson(
                errorBody,
                Resource.class);

        return errorResponse;
    }

    protected void onFetchFailed() {
    }

    public LiveData<Resource<ResultType>> asLiveData() {
        return result;
    }

    @WorkerThread
    protected RequestType processResponse(ApiResponse<RequestType> response) {
        return response.data;
    }

    @WorkerThread
    protected void saveCallResult(@NonNull RequestType item) {

    }

    @MainThread
    protected boolean shouldFetch(@Nullable ResultType data) {
        return false;
    }

    @NonNull
    @WorkerThread
    protected abstract Call<ApiResponse<RequestType>> createCall();
}