package com.example.healthcoins.domain.user.participant;

import com.example.healthcoins.domain.user.PersonData;

public class Participant extends PersonData {

    public Participant(String id, String dateOfBirth, String firstName, String lastName, String policyNumber, String phoneNumber, String gender, String address) {
        super(id, dateOfBirth, firstName, lastName, policyNumber, phoneNumber, gender, address);
    }
}
