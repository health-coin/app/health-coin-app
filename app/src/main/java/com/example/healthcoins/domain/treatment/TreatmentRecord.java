package com.example.healthcoins.domain.treatment;

import com.example.healthcoins.domain.treatment.contract.TreatmentContract;

/**
 * List result
 */
public class TreatmentRecord {
    private Treatment treatment;
    private TreatmentContract contract;

    public Treatment getTreatment() {
        return treatment;
    }

    public void setTreatment(Treatment treatment) {
        this.treatment = treatment;
    }

    public TreatmentContract getContract() {
        return contract;
    }

    public void setContract(TreatmentContract contract) {
        this.contract = contract;
    }
}
