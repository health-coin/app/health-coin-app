package com.example.healthcoins;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.NavGraph;
import androidx.navigation.NavInflater;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;

import com.example.healthcoins.databinding.ActivityMainBinding;
import com.example.healthcoins.databinding.NavHeaderMainBinding;
import com.example.healthcoins.domain.auth.TokenData;
import com.example.healthcoins.pages.auth.AuthActivity;
import com.example.healthcoins.services.TokenService;
import com.example.healthcoins.services.UserService;
import com.example.healthcoins.utils.Constants;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private NavController navController;
    private DrawerLayout drawer;
    private MainViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        NavHeaderMainBinding navHeaderMainBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.nav_header_main, binding.participantView, false);
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        drawer = findViewById(R.id.drawer_layout);
        viewModel = new MainViewModel();
        viewModel.setUserData(UserService.getInstance(this).getUser());
        binding.setViewModel(viewModel);
        binding.setCallback(this);
        navHeaderMainBinding.setViewModel(viewModel);
        binding.participantView.addHeaderView(navHeaderMainBinding.getRoot());

        navController = navHostFragment.getNavController();
        this.determineNavigationFlow(navHostFragment);
        this.setNavigationUI();
    }

    private void setNavigationUI() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        NavigationView navView = findViewById(R.id.participant_view);
        navView.setNavigationItemSelectedListener(this);
        NavigationUI.setupWithNavController(toolbar, navController, drawer);
        NavigationUI.setupWithNavController(navView, navController);
    }

    private void determineNavigationFlow(NavHostFragment navHostFragment) {
        boolean isParticipant = this.isParticipant();
        viewModel.setIsParticipant(isParticipant);
        NavInflater inflater = navHostFragment.getNavController().getNavInflater();
        NavGraph graph = inflater.inflate(R.navigation.main_graph);
        if (!isParticipant) {
            graph.setStartDestination(R.id.careproviderHome);
            navController.setGraph(graph);
        }
    }

    public boolean isParticipant() {
        String role = UserService.getInstance(this).getUser().getRole();
        return role.equals(Constants.PARTICIPANT_ROLE);
    }

    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(navController, drawer);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        drawer.closeDrawer(GravityCompat.START);
        return NavigationUI.onNavDestinationSelected(item, navController)
                || super.onOptionsItemSelected(item);
    }

    public void logout(View view) {
        TokenService.getInstance(this).setToken(new TokenData(null, null, null));
        Intent intent = new Intent(this, AuthActivity.class);
        startActivity(intent);
    }
}
