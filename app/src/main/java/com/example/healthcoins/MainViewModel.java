package com.example.healthcoins;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.support.design.widget.NavigationView;
import android.view.View;
import com.android.databinding.library.baseAdapters.BR;
import com.example.healthcoins.domain.user.UserData;

public class MainViewModel extends BaseObservable {
    private boolean isParticipant = false;
    private UserData userData;

    @Bindable
    public Boolean getIsParticipant() {
        return isParticipant;
    }

    @Bindable
    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData value) {
        if (userData != value) {
            userData = value;
        }
    }

    public void setIsParticipant(Boolean value) {
        if (isParticipant != value) {
            isParticipant = value;
            notifyPropertyChanged(BR.isParticipant);
        }
    }

    @Bindable
    public int getOnlyVisibleForParticipant() {
        return getIsParticipant() ? View.VISIBLE : View.GONE;
    }

    public int onlyVisibleForCareProvider() {
        return !getIsParticipant() ? View.VISIBLE : View.GONE;
    }

    @BindingAdapter("app:menu")
    public static void setMenu(NavigationView navigationView, int id) {
        navigationView.inflateMenu(id);
    }
}
