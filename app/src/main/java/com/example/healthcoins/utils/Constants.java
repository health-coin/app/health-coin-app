package com.example.healthcoins.utils;

public class Constants {

    public static final String TREATMENT_BASE_TO_MEASUREMENT_KEY =  "measurementList";
    public static String AUTHORIZATION_PREFIX = "Bearer ";
    public static String PARTICIPANT_ROLE = "Participant";

}
