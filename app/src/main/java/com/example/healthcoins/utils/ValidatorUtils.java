package com.example.healthcoins.utils;

import android.widget.Spinner;
import android.widget.TextView;

import com.example.healthcoins.validators.NumberViewValidator;
import com.example.healthcoins.validators.SpinnerValidator;
import com.example.healthcoins.validators.TextViewValidator;

import java.util.List;

public class ValidatorUtils {

    public static boolean validateTextViewValidations(TextViewValidator validator, List<TextView> textViews) {
        for (TextView textView : textViews) {
            if (!validator.validate(textView)) {
                return false;
            }
        }
        return true;
    }

    public static boolean validateNumberViewValidations(NumberViewValidator validator, List<TextView> textViews) {
        for (TextView textView : textViews) {
            if (!validator.validate(textView)) {
                return false;
            }
        }
        return true;
    }

    public static boolean validateSpinnerValidations(SpinnerValidator validator, List<Spinner> spinners) {
        for (Spinner spinner : spinners) {
            if (!validator.validate(spinner)) {
                return false;
            }
        }
        return true;
    }
}
