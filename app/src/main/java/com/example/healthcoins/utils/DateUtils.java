package com.example.healthcoins.utils;

import android.text.format.DateFormat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    public static String formatDateOnYear(Date date) {
        return DateFormat.format(DATE_FORMAT, date).toString();
    }


}
