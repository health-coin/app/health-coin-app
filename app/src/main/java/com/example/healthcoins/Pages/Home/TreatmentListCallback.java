package com.example.healthcoins.pages.home;

import com.example.healthcoins.domain.treatment.TreatmentRecord;

public interface TreatmentListCallback {
    void onSignedTreatmentClicked(TreatmentRecord record);
    void onStartedTreatmentClicked(TreatmentRecord record);
    void onDoneTreatmentClicked(TreatmentRecord record);
}
