package com.example.healthcoins.pages.treatments.measurements;

import android.content.Context;
import android.database.DataSetObserver;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.TreatmentGoalListrowBinding;
import com.example.healthcoins.domain.treatment.measurement.Measurement;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;
import com.example.healthcoins.pages.treatments.TreatmentListViewValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExpandableAdapter extends BaseExpandableListAdapter {
    private List<String> groupList;
    private List<TreatmentListViewValue> childListMap;
    private Context activity;

    public ExpandableAdapter(Context activity, List<String> groupList, List<TreatmentListViewValue> childListMap) {
        this.activity = activity;
        this.groupList = groupList;
        this.childListMap = childListMap;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public int getGroupCount() {
        return groupList.size();
    }

    @Override
    public int getChildrenCount(int groupIndex) {
       // String group = groupList.get(groupIndex);
        List<String> childInfoList = (List<String>) childListMap.get(groupIndex);
        return childInfoList.size();
    }

    @Override
    public Object getGroup(int groupIndex) {
        return groupList.get(groupIndex);
    }

    @Override
    public Object getChild(int groupIndex, int childIndex) {
        //  String group = groupList.get(groupIndex);
        List<String> childInfoList = (List<String>) childListMap.get(groupIndex);
        return childInfoList.get(childIndex);
    }

    @Override
    public long getGroupId(int groupIndex) {
        return groupIndex;
    }

    @Override
    public long getChildId(int groupIndex, int childIndex) {
        return childIndex;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupIndex, boolean isExpanded, View view, ViewGroup viewGroup) {
        LinearLayout groupLayoutView = new LinearLayout(activity);
        groupLayoutView.setOrientation(LinearLayout.HORIZONTAL);


        // Create and add a textview in returned group view.
        String groupText = groupList.get(groupIndex);
        TextView groupTextView = new TextView(activity);
        groupTextView.setText(groupText);
        groupTextView.setTextSize(25);
        groupLayoutView.setPadding(90,0,0,0);
        groupLayoutView.addView(groupTextView);


        return groupLayoutView;
    }

    @Override
    public View getChildView(int groupIndex, int childIndex, boolean isLastChild, View view, ViewGroup viewGroup) {
        TreatmentListViewValue value = (TreatmentListViewValue) this.getChild(groupIndex, childIndex);
        String childText = String.valueOf(value);

        // Create a TextView to display child text.
        TextView childTextView = new TextView(activity);
        childTextView.setText(childText);
        childTextView.setTextSize(20);
        childTextView.setBackgroundColor(Color.rgb(255, 223, 221));
        childTextView.setPadding(135,0,0,0);

        return childTextView;
    }



    @Override
    public boolean isChildSelectable(int groupIndex, int childIndex) {
        return false;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void onGroupExpanded(int groupIndex) {

    }

    @Override
    public void onGroupCollapsed(int groupIndex) {

    }

    @Override
    public long getCombinedChildId(long groupIndex, long childIndex) {
        return 0;
    }

    @Override
    public long getCombinedGroupId(long groupIndex) {
        return 0;
    }

}

