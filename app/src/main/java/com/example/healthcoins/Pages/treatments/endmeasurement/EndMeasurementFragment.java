package com.example.healthcoins.pages.treatments.endmeasurement;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.EndMeasurementFragmentBinding;
import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.treatment.measurement.Measurement;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;
import com.example.healthcoins.domain.user.participant.Participant;
import com.example.healthcoins.pages.UserViewModel;
import com.example.healthcoins.pages.treatments.GoalListAdapter;
import com.example.healthcoins.pages.treatments.TreatmentListViewAdapter;
import com.example.healthcoins.pages.treatments.start.ModifyStartMeasurementListAdapter;
import com.example.healthcoins.repository.TreatmentRepository;
import com.example.healthcoins.services.TokenService;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class EndMeasurementFragment extends Fragment {

    private EndMeasurementViewModel mViewModel;
    private UserViewModel userViewModel;
    private EndMeasurementFragmentBinding endMeasurementFragmentBinding;
    private TreatmentRepository treatmentRepository;
    private List<MeasurementValue> measurementValueList;
    private String accessToken;
    private NavController navController;
    public static EndMeasurementFragment newInstance() {
        return new EndMeasurementFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        endMeasurementFragmentBinding = DataBindingUtil.inflate(
                inflater, R.layout.end_measurement_fragment, container, false);
        mViewModel = ViewModelProviders.of(this).get(EndMeasurementViewModel.class);
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        accessToken = TokenService.getInstance(getActivity()).getToken().getAccessToken();
        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        treatmentRepository = new TreatmentRepository();

        endMeasurementFragmentBinding.setIsParticipantIsLoading(true);
        endMeasurementFragmentBinding.setTreatmentIsLoading(true);
        endMeasurementFragmentBinding.setCallback(this);
        loadTreatmentTreatment();
        return endMeasurementFragmentBinding.getRoot();
    }

    private void loadTreatmentTreatment() {
        String treatmentid = getTreatmentId();
        mViewModel.getTreatmentById(treatmentid, accessToken).observe(this, treatmentDetailResource -> {
            if(treatmentDetailResource != null && treatmentDetailResource.data != null) {
                endMeasurementFragmentBinding.setTreatmentDetail(treatmentDetailResource.data);
                loadUserViewModel();
                setBehandelplanList();
                setLatestMeasurementValues();
                setCurrentMeasurement();
            }
            endMeasurementFragmentBinding.setTreatmentIsLoading(false);

        });
    }
    private void setBehandelplanList() {
        ListView behandelPlanList = endMeasurementFragmentBinding.startTreatmentGoallist;
        List<MeasurementValue> measurementValueList = Arrays.asList(endMeasurementFragmentBinding.getTreatmentDetail().getGoalMeasurement());
        GoalListAdapter goalListAdapter = new GoalListAdapter(getActivity(), measurementValueList);

        behandelPlanList.setAdapter(goalListAdapter);
    }

    private void loadUserViewModel() {
        userViewModel.getParticipantData(endMeasurementFragmentBinding.getTreatmentDetail().getContractData().getParticipant(), accessToken).observe(this, this::handleParticipantResponse);
    }

    private void handleParticipantResponse(Resource<Participant> participantResource) {
        if(participantResource != null && participantResource.data != null) {
            endMeasurementFragmentBinding.setParticipant(participantResource.data);
        }
        endMeasurementFragmentBinding.setIsParticipantIsLoading(false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // TODO: Use the ViewModel
    }

    private String getTreatmentId() {
        if(getArguments() == null) {
            Toasty.error(getActivity(), "Kon behandelplan niet ophalen", Toasty.LENGTH_LONG).show();
            return "";
        }
        return getArguments().getString("treatmentId");
    }

    private Measurement getLatestMeasurement() {

        if (endMeasurementFragmentBinding.getTreatmentDetail().hasCustomMeasurement()) {
            Measurement[] customMeasurements = endMeasurementFragmentBinding.getTreatmentDetail().getCustomMeasurements();
            return customMeasurements[customMeasurements.length - 1];
        }
        return createEndMeasurement();
    }

    private Measurement createEndMeasurement() {
        Date endDate = new Date(endMeasurementFragmentBinding.getTreatmentDetail().getContractData().getPlannedEndDate());
        Measurement endMeasurement = new Measurement();
        endMeasurement.setDate(endDate);
        endMeasurement.setDoneByProfessional(true);
        for (MeasurementValue value : endMeasurementFragmentBinding.getTreatmentDetail().getEndMeasurement()) {
            endMeasurement.addValue(value);
        }
        return endMeasurement;
    }

    private void setLatestMeasurementValues() {
        ListView latestMeasurementsView = endMeasurementFragmentBinding.lastMeasurementList;
        Measurement latestMeasurement = getLatestMeasurement();
        if (!latestMeasurement.hasMeasurements()) {
            endMeasurementFragmentBinding.setHasLatestMeasurement(false);
            return;
        }

        TreatmentListViewAdapter listViewAdapter = new GoalListAdapter(getActivity(), latestMeasurement.getValues());
        latestMeasurementsView.setAdapter(listViewAdapter);
        endMeasurementFragmentBinding.setHasLatestMeasurement(true);
    }

    private void setCurrentMeasurement() {
        ListView currentMeasurementView = endMeasurementFragmentBinding.currentMeasurement;
        measurementValueList = Arrays.asList(endMeasurementFragmentBinding.getTreatmentDetail().getGoalMeasurement());
        ModifyStartMeasurementListAdapter listAdapter = new ModifyStartMeasurementListAdapter(getActivity(), measurementValueList);
        currentMeasurementView.setAdapter(listAdapter);
    }

    public void addEndMeasurement(View view) {
        MeasurementValue[] measurementValues = measurementValueList.toArray(new MeasurementValue[0]);
        treatmentRepository.createMeasurement(measurementValues, this.getTreatmentId(), accessToken).observe(this, signTreatmentResponse -> {
            if(signTreatmentResponse.code == null) {
                Toasty.success(getActivity(), signTreatmentResponse.message, Toasty.LENGTH_LONG).show();
                navController.popBackStack();

            } else {
                Toasty.error(getActivity(), signTreatmentResponse.message, Toasty.LENGTH_LONG).show();
            }
        });
    }

}
