package com.example.healthcoins.pages.treatments.measurements;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.MeasurementHomeViewFragmentBinding;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.domain.treatment.measurement.Measurement;
import com.example.healthcoins.pages.treatments.TreatmentBaseViewModel;
import com.example.healthcoins.pages.treatments.stepregister.TreatmentHomeViewModel;
import com.example.healthcoins.utils.DateUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MeasurementsHome extends Fragment {

    private TreatmentDetail treatmentDetail;
    private TreatmentBaseViewModel treatmentBaseViewModel;
    private MeasurementHomeViewFragmentBinding binding;
    private List<String> dateList = new ArrayList<>();
    private List<Measurement> currentMeasurements;
    private TreatmentHomeViewModel treatmentHomeViewModel;
    private NavController navController;

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        dateList.clear();
        initializeViewModel();
        binding = DataBindingUtil.inflate(inflater, R.layout.measurements_overview_fragment, container, false);
        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding.setHasError(false);
        binding.setIsLoading(true);

        treatmentBaseViewModel.getSelectedTreatmentDetail().observe(getActivity(), treatmentResponse -> {
            treatmentDetail = treatmentResponse;
            binding.setTreatmentDetail(treatmentDetail);
            binding.setHasError(true);
            loadCusMeasurementData();
            binding.setIsLoading(false);
        });

        loadMeasurementsButton();
    }

    private void loadCusMeasurementData() {
        if (treatmentDetail == null) {
            return;
        }

        currentMeasurements =  new ArrayList<>(Arrays.asList(treatmentDetail.getTreatmentData().getCustomMeasurements()));
        treatmentHomeViewModel.setSelectMeasurements(currentMeasurements);
            setCusMeasurementValues();
    }

    private void setCusMeasurementValues() {
        ListView lv = binding.dataMeasurementList;
        Measurement[] customMeasurements = treatmentDetail.getCustomMeasurements();

            for (Measurement customMeasurement : customMeasurements) {
                dateList.add(DateUtils.formatDateOnYear(customMeasurement.getDate()));
            }

            if(getActivity() != null) {
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.measurement_daterow, R.id.datevalue, dateList);
                lv.setAdapter(arrayAdapter);
            }
    }

    private void loadMeasurementsButton(){
       binding.dataMeasurementView.setOnClickListener((View view) -> {
            Bundle bundle = new Bundle();
            navController.navigate(R.id.measurementsDetails, bundle);
        });
    }


    private void initializeViewModel() {
        treatmentBaseViewModel = ViewModelProviders.of(getActivity()).get(TreatmentBaseViewModel.class);
        treatmentHomeViewModel = ViewModelProviders.of(getActivity()).get(TreatmentHomeViewModel.class); }
}

