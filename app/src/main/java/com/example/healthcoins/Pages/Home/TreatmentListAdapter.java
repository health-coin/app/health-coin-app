package com.example.healthcoins.pages.home;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.TreatmentListrowBinding;
import com.example.healthcoins.domain.treatment.TreatmentRecord;
import com.example.healthcoins.domain.treatment.TreatmentState;
import com.example.healthcoins.domain.treatment.contract.TreatmentContract;

import java.util.List;

public class TreatmentListAdapter extends BaseAdapter {

    private List<TreatmentRecord> treatments;
    private Context activity;
    private TreatmentListCallback treatmentListCallback;

    public TreatmentListAdapter(Context activity, List<TreatmentRecord> treatments, TreatmentListCallback treatmentListCallback) {
        this.treatments = treatments;
        this.activity = activity;
        this.treatmentListCallback = treatmentListCallback;
    }

    @Override
    public int getCount() {
        return treatments.size();
    }

    @Override
    public Object getItem(int position) {
        return treatments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TreatmentListrowBinding binding;
        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.treatment_listrow, null);
            binding = DataBindingUtil.bind(convertView);

            convertView.setTag(binding);
        } else {
            binding = (TreatmentListrowBinding) convertView.getTag();
        }
        binding.setCallback(this);
        binding.setModel(new TreatmentListBindingModel());
        binding.setTreatmentRecord(treatments.get(position));
        return binding.getRoot();
    }

    public void onTreatmentPressed(View view, TreatmentRecord treatmentRecord) {
        if(treatmentListCallback != null) {
            if (treatmentRecord.getContract().isSigned()) {
                this.treatmentListCallback.onSignedTreatmentClicked(treatmentRecord);
            }
            if (treatmentRecord.getContract().isStarted()) {
                this.treatmentListCallback.onStartedTreatmentClicked(treatmentRecord);
            }
            if(treatmentRecord.getContract().isDone()) {
                this.treatmentListCallback.onDoneTreatmentClicked(treatmentRecord);
            }
        }
    }

}
