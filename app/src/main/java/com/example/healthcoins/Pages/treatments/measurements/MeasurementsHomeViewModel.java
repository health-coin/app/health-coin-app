package com.example.healthcoins.pages.treatments.measurements;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.domain.treatment.measurement.Measurement;
import com.example.healthcoins.domain.treatment.TreatmentRecord;
import com.example.healthcoins.repository.TreatmentRepository;

import java.util.List;

/**
 * ViewModel abstraction over Repository to support live data for UI.
 */
public class MeasurementsHomeViewModel extends ViewModel {

    public MeasurementsHomeViewModel(){}

    private LiveData<Resource<TreatmentDetail>> treatmentData;
    private TreatmentRepository treatmentRepository = new TreatmentRepository();

    void init(String bearerToken) {
        if (treatmentData != null) {
            return;
        }
        this.treatmentData = treatmentRepository.getActiveTreatment(bearerToken);
    }

    LiveData<Resource<TreatmentDetail>> getActiveTreatment() {
        return treatmentData;
    }
}
