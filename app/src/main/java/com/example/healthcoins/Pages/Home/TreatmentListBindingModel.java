package com.example.healthcoins.pages.home;

import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.view.View;

import com.example.healthcoins.domain.treatment.TreatmentRecord;
import com.example.healthcoins.domain.treatment.TreatmentState;

public class TreatmentListBindingModel extends BaseObservable {

    @BindingAdapter("android:visibility")
    public static void setVisibility(View view, Boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    public boolean isPending(TreatmentRecord treatmentRecord) {
        return TreatmentState.valueOf(treatmentRecord.getContract().getState()).equals(TreatmentState.PENDING);
    }
}
