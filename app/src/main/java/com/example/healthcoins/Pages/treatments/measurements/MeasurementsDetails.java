package com.example.healthcoins.pages.treatments.measurements;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.MeasurementDetailsHomeViewFragmentBinding;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.domain.treatment.measurement.Measurement;
import com.example.healthcoins.pages.treatments.GoalListAdapter;
import com.example.healthcoins.pages.treatments.TreatmentBaseViewModel;
import com.example.healthcoins.pages.treatments.TreatmentListViewAdapter;
import com.example.healthcoins.pages.treatments.TreatmentListViewValue;
import com.example.healthcoins.pages.treatments.stepregister.TreatmentHomeViewModel;
import com.example.healthcoins.utils.DateUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class MeasurementsDetails extends Fragment {

    private TreatmentDetail treatmentDetail;
    private TreatmentBaseViewModel treatmentBaseViewModel;
    private MeasurementDetailsHomeViewFragmentBinding binding;
    private TreatmentHomeViewModel treatmentHomeViewModel;
    private NavController navController;
    List<Measurement> currentMeasurements;

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        initializeViewModel();
        binding = DataBindingUtil.inflate(inflater, R.layout.measurement_details, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding.setHasError(false);
        binding.setIsLoading(true);

        treatmentBaseViewModel.getSelectedTreatmentDetail().observe(getActivity(), treatmentResponse -> {
            treatmentDetail = treatmentResponse;
            binding.setTreatmentDetail(treatmentDetail);
            binding.setHasError(true);
            loadTreatmentPlanData();
            binding.setIsLoading(false);
        });
    }

    private void loadTreatmentPlanData() {
        if (treatmentDetail == null) {
            return;
        }
        currentMeasurements =  new ArrayList<>(Arrays.asList(treatmentDetail.getTreatmentData().getCustomMeasurements()));
        treatmentHomeViewModel.setSelectMeasurements(currentMeasurements);
        setMeasurementDetailsValues();
    }

    private void setMeasurementDetailsValues() {
        ListView measurementsView = binding.lastMeasurementList;
        Measurement measurementDetails = getMeasurementDetails();
        if (!measurementDetails.hasMeasurements()) {
            binding.setHasMeasurement(false);
            return;
        }

        TreatmentListViewAdapter listViewAdapter = new GoalListAdapter(getActivity(), measurementDetails.getValues());

        listViewAdapter.addValue(new TreatmentListViewValue("Datum", "=", DateUtils.formatDateOnYear(measurementDetails.getDate())));
        String doneByProfessional = measurementDetails.isDoneByProfessional() ? "Ja" : "Nee";
        listViewAdapter.addValue(new TreatmentListViewValue("Door specialist ", "=", doneByProfessional));
        measurementsView.setAdapter(listViewAdapter);
    }

    private Measurement getMeasurementDetails() {
            return currentMeasurements.get(currentMeasurements.size() - 1);
    }



    private void initializeViewModel() {
        treatmentBaseViewModel = ViewModelProviders.of(getActivity()).get(TreatmentBaseViewModel.class);
        treatmentHomeViewModel = ViewModelProviders.of(getActivity()).get(TreatmentHomeViewModel.class);
    }
}
