package com.example.healthcoins.pages.treatments.measurements;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.MeasurementDetailsHomeViewFragmentBinding;
import com.example.healthcoins.pages.treatments.TreatmentListViewValue;

import java.util.List;

public class CustomMeasurementsAdapter extends BaseAdapter {

    private Context activity;
    private List<TreatmentListViewValue> treatmentValue;

    public CustomMeasurementsAdapter(Context activity, List<TreatmentListViewValue> treatmentValue) {
        this.activity = activity;
        this.treatmentValue = treatmentValue;
    }

    public void addValue(TreatmentListViewValue value) {
        treatmentValue.add(value);
    }

    @Override
    public int getCount() {
        return treatmentValue.size();
    }

    @Override
    public Object getItem(int position) {
        return treatmentValue.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            MeasurementDetailsHomeViewFragmentBinding binding;
            if (convertView == null) {
                convertView = LayoutInflater.from(activity).inflate(R.layout.treatment_goal_listrow, null);
                binding = DataBindingUtil.bind(convertView);

                convertView.setTag(binding);
            } else {
                binding = (MeasurementDetailsHomeViewFragmentBinding) convertView.getTag();
            }
            //binding.setCallback(this);
          //  binding.setListValue(treatmentValue.get(position));
            return binding.getRoot();
    }

}
