package com.example.healthcoins.pages.treatments.stepsign;

import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.StepSignAfterEndMeasurementFragmentBinding;
import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.domain.treatment.measurement.Measurement;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;
import com.example.healthcoins.domain.user.careprofessional.CareProfessional;
import com.example.healthcoins.pages.UserViewModel;
import com.example.healthcoins.pages.treatments.GoalListAdapter;
import com.example.healthcoins.pages.treatments.TreatmentBaseViewModel;
import com.example.healthcoins.pages.treatments.TreatmentListViewAdapter;
import com.example.healthcoins.pages.treatments.TreatmentListViewValue;
import com.example.healthcoins.pages.treatments.accept.AcceptTreatmentBindingModel;
import com.example.healthcoins.repository.TreatmentRepository;
import com.example.healthcoins.services.TokenService;

import java.util.Arrays;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class StepSignAfterEndMeasurementFragment extends Fragment {

    private TreatmentBaseViewModel baseViewModel;
    private UserViewModel userViewModel;
    private String accessToken;
    private TreatmentRepository repository;
    private StepSignAfterEndMeasurementFragmentBinding viewBinding;
    private NavController navController;

    public static StepSignAfterEndMeasurementFragment newInstance() {
        return new StepSignAfterEndMeasurementFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        viewBinding = DataBindingUtil.inflate(inflater, R.layout.step_sign_after_end_measurement_fragment, container, false);
        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

        accessToken = TokenService.getInstance(getActivity()).getToken().getAccessToken();
        repository = new TreatmentRepository();
        setInitialBindingData();
        loadTreatmentData();
        return viewBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void setInitialBindingData() {
        viewBinding.setIsLoadingParticipant(true);
        viewBinding.setIsLoadingTreatmentDetail(true);
        viewBinding.setCallback(this);
    }

    private void handleTreatmentDetailResponse(TreatmentDetail treatmentDetail) {
        viewBinding.setTreatmentDetail(treatmentDetail);
        viewBinding.setIsLoadingTreatmentDetail(false);
        setBehandelplanList();
        setFinalResults();
        loadUserData(treatmentDetail.getContractData().getCareTaker());
    }

    private void loadTreatmentData() {
        baseViewModel = ViewModelProviders.of(getActivity()).get(TreatmentBaseViewModel.class);
        baseViewModel.getSelectedTreatmentDetail().observe(getActivity(), this::handleTreatmentDetailResponse);

    }

    private void loadUserData(String careProfessionalAddress) {
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        userViewModel.getCareProfessionalData(careProfessionalAddress, accessToken).observe(this, this::handleCareProfessionalResponse);

    }

    private void handleCareProfessionalResponse(Resource<CareProfessional> careProfessionalResource) {
        if(careProfessionalResource.data != null) {
            viewBinding.setCareProfessional(careProfessionalResource.data);
        } else {
            Toasty.error(getActivity(), careProfessionalResource.message, Toasty.LENGTH_LONG).show();
        }
        viewBinding.setIsLoadingParticipant(false);

    }

    private void setFinalResults() {
        ListView latestMeasurementsView = viewBinding.archievedResultList;
        List<MeasurementValue> measurementValueList = Arrays.asList(viewBinding.getTreatmentDetail().getEndMeasurement());
        TreatmentListViewAdapter listViewAdapter = new GoalListAdapter(getActivity(), measurementValueList);
        latestMeasurementsView.setAdapter(listViewAdapter);
    }

    private void setBehandelplanList() {
        ListView behandelPlanList = viewBinding.startTreatmentGoallist;
        List<MeasurementValue> measurementValueList = Arrays.asList(viewBinding.getTreatmentDetail().getGoalMeasurement());
        TreatmentListViewAdapter listAdapter = new GoalListAdapter(getActivity(), measurementValueList);
        Integer coinReward = viewBinding.getTreatmentDetail().getPendingCoins();
        TreatmentListViewValue creditValue = new TreatmentListViewValue("Coins verdiend", ":", "" + coinReward);
        listAdapter.addValue(creditValue);
        behandelPlanList.setAdapter(listAdapter);
    }

    public void onAcceptClicked(View view) {
        ProgressDialog dialog = ProgressDialog.show(getActivity(), "Laden",
                "Eindmeting signeren...", true);
        repository.acceptTreatment(viewBinding.getTreatmentDetail().getTreatmentData().getId(), accessToken).observe(this, objectResource -> {
            if(objectResource.code == null) {
                Toasty.success(getActivity(), objectResource.message, Toasty.LENGTH_LONG).show();
                navController.popBackStack();
            } else {
                Toasty.error(getActivity(), objectResource.message, Toasty.LENGTH_LONG).show();
            }
            dialog.hide();
        });
    }
}
