package com.example.healthcoins.pages.treatments.endmeasurement;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.repository.TreatmentRepository;

public class EndMeasurementViewModel extends ViewModel {
    private TreatmentRepository treatmentRepository = new TreatmentRepository();
    public LiveData<Resource<TreatmentDetail>> getTreatmentById(String id, String accessToken) {
        return treatmentRepository.getTreatmentPlanById(id, accessToken);
    }
}
