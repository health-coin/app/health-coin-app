package com.example.healthcoins.pages.treatments.measurements;

import android.content.Context;

import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;
import com.example.healthcoins.pages.treatments.TreatmentListViewValue;

import java.util.ArrayList;
import java.util.List;

public class CusMeasurementsListAdapter extends CustomMeasurementsAdapter {

    public CusMeasurementsListAdapter(Context activity, List<MeasurementValue> measurementValues) {
        super(activity, createListViewValues(measurementValues));
    }

    private static List<TreatmentListViewValue> createListViewValues(List<MeasurementValue> measurementValues) {
        List<TreatmentListViewValue> treatmentValues = new ArrayList<>();
        for (MeasurementValue value : measurementValues) {
            treatmentValues.add(new TreatmentListViewValue(value.getUnit(), value.getComparisonText(), value.getQuantity() + ""));
        }
        return treatmentValues;
    }
}
