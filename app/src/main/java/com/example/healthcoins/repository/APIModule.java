package com.example.healthcoins.repository;

import com.example.healthcoins.BuildConfig;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class APIModule {

    private static APIModule instance;

    private APIModule() {

    }

    HealthCoinAPI provideAPI() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(HealthCoinAPI.class);
    }

    static APIModule getInstance() {
        if (instance == null) {
            instance = new APIModule();
        }
        return instance;
    }
}
