package com.example.healthcoins.repository;

import com.example.healthcoins.domain.ApiResponse;
import com.example.healthcoins.domain.transaction.BalanceData;
import com.example.healthcoins.domain.treatment.CreateTreatmentRequest;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.domain.treatment.TreatmentRecord;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;
import com.example.healthcoins.domain.user.careprofessional.CareProfessional;
import com.example.healthcoins.domain.user.participant.Participant;
import com.example.healthcoins.domain.user.participant.ParticipantRequest;
import com.example.healthcoins.domain.user.UserData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface HealthCoinAPI {

    @GET("/user/balance")
    Call<ApiResponse<BalanceData>> getBalance(@Header("Authorization") String token);

    @GET("/treatmentplans/active")
    Call<ApiResponse<TreatmentDetail>> getActiveTreatment(@Header("Authorization") String token);

    @POST("/treatmentplans/{treatmentId}/sign")
    Call<ApiResponse<Object>> acceptTreatment(@Path ("treatmentId") String treatmentId, @Header("Authorization") String token);

    @GET("/treatmentplans/{treatmentId}")
    Call<ApiResponse<TreatmentDetail>> getTreatmentById(@Path ("treatmentId") String treatmentId, @Header("Authorization") String token);

    @GET("/treatmentplans")
    Call<ApiResponse<TreatmentRecord[]>> getTreatmentplans(@Query("state") String state, @Header("Authorization") String token);

    @POST("/treatmentplans/{treatmentId}/measurements")
    Call<ApiResponse<Object>> createMeasurement(@Body MeasurementValue[] measurementValues, @Path ("treatmentId") String treatmentId, @Header("Authorization") String token);

    @POST("/treatmentplans")
    Call<ApiResponse<Object>> createTreatmentplan(@Body CreateTreatmentRequest request, @Header("Authorization") String token);

    @FormUrlEncoded
    @POST("/login")
    Call<ApiResponse<UserData>> loginUser(@Field("email") String email, @Field("password") String password);

    @POST("/participants")
    Call<ApiResponse<Participant>> createParticipant(@Body ParticipantRequest participantRequest);

    @GET("/participant/careprofessionals/{address}")
    Call<ApiResponse<CareProfessional>> getCareProfessionalByAddress(@Path("address") String address, @Header("Authorization") String token);

    @GET("/careprofessional/participants/{address}")
    Call<ApiResponse<Participant>> getParticipantByAddress(@Path("address") String address, @Header("Authorization") String token);

    @GET("/participants")
    Call<ApiResponse<Participant[]>> getParticipants(@Header("Authorization") String token);
}