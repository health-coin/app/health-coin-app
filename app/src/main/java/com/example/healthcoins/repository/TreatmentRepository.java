package com.example.healthcoins.repository;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.healthcoins.domain.ApiResponse;
import com.example.healthcoins.domain.NetworkBoundResource;
import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.treatment.CreateTreatmentRequest;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.domain.treatment.TreatmentRecord;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;
import com.example.healthcoins.utils.Constants;

import retrofit2.Call;

public class TreatmentRepository {

    private HealthCoinAPI webservice;

    public TreatmentRepository() {
        webservice = APIModule.getInstance().provideAPI();
    }

    public LiveData<Resource<TreatmentDetail>> getActiveTreatment(String accessToken) {
        return new NetworkBoundResource<TreatmentDetail, TreatmentDetail>() {
            @NonNull
            @Override
            protected Call<ApiResponse<TreatmentDetail>> createCall() {
                return webservice.getActiveTreatment(Constants.AUTHORIZATION_PREFIX + accessToken);
            }
        }.asLiveData();
    }

    public LiveData<Resource<Object>> acceptTreatment(String treatmentId, String accessToken) {
        return new NetworkBoundResource<Object, Object>() {
            @NonNull
            @Override
            protected Call<ApiResponse<Object>> createCall() {
                return webservice.acceptTreatment(treatmentId, Constants.AUTHORIZATION_PREFIX + accessToken);
            }
        }.asLiveData();
    }

    public LiveData<Resource<TreatmentRecord[]>> getTreatmentPlans(String state, String accessToken) {
        return new NetworkBoundResource<TreatmentRecord[], TreatmentRecord[]>() {
            @NonNull
            @Override
            protected Call<ApiResponse<TreatmentRecord[]>> createCall() {
                return webservice.getTreatmentplans(state, Constants.AUTHORIZATION_PREFIX + accessToken);
            }
        }.asLiveData();
    }

    public LiveData<Resource<Object>> createTreatmentPlan(CreateTreatmentRequest request, String accessToken) {
        return new NetworkBoundResource<Object, Object>() {
            @NonNull
            @Override
            protected Call<ApiResponse<Object>> createCall() {
                return webservice.createTreatmentplan(request, Constants.AUTHORIZATION_PREFIX + accessToken);
            }
        }.asLiveData();
    }

    public LiveData<Resource<TreatmentDetail>> getTreatmentPlanById(String treatmentId, String accessToken) {
        return new NetworkBoundResource<TreatmentDetail, TreatmentDetail>() {
            @NonNull
            @Override
            protected Call<ApiResponse<TreatmentDetail>> createCall() {
                return webservice.getTreatmentById(treatmentId, Constants.AUTHORIZATION_PREFIX + accessToken);
            }
        }.asLiveData();
    }

    public LiveData<Resource<Object>> createMeasurement(MeasurementValue[] request, String treatmentId, String accessToken) {
        return new NetworkBoundResource<Object, Object>() {
            @NonNull
            @Override
            protected Call<ApiResponse<Object>> createCall() {
                return webservice.createMeasurement(request, treatmentId, Constants.AUTHORIZATION_PREFIX + accessToken);
            }
        }.asLiveData();
    }
}
