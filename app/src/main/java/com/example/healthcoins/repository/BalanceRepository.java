package com.example.healthcoins.repository;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.healthcoins.domain.ApiResponse;
import com.example.healthcoins.domain.NetworkBoundResource;
import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.transaction.BalanceData;
import com.example.healthcoins.utils.Constants;

import javax.inject.Singleton;

import retrofit2.Call;

@Singleton
public class BalanceRepository {

    private HealthCoinAPI webservice;

    public BalanceRepository() {
        webservice = APIModule.getInstance().provideAPI();
    }

    public LiveData<Resource<BalanceData>> getBalance(String accessToken) {
        return new NetworkBoundResource<BalanceData, BalanceData>() {
            @NonNull
            @Override
            protected Call<ApiResponse<BalanceData>> createCall() {
                return webservice.getBalance(Constants.AUTHORIZATION_PREFIX + accessToken);

            }
        }.asLiveData();
    }
}
