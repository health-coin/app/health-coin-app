package com.example.healthcoins.repository;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.healthcoins.domain.ApiResponse;
import com.example.healthcoins.domain.NetworkBoundResource;
import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.user.UserData;

import retrofit2.Call;

public class AuthRepository {

    private HealthCoinAPI webservice;

    public AuthRepository() {
        webservice = APIModule.getInstance().provideAPI();
    }

    public LiveData<Resource<UserData>> loginUser(String email, String password) {
        return new NetworkBoundResource<UserData, UserData>() {
            @NonNull
            @Override
            protected Call<ApiResponse<UserData>> createCall() {
                return webservice.loginUser(email, password);
            }
        }.asLiveData();
    }
}
