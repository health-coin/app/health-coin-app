package com.example.healthcoins.repository;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.healthcoins.domain.ApiResponse;
import com.example.healthcoins.domain.NetworkBoundResource;
import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.user.careprofessional.CareProfessional;
import com.example.healthcoins.domain.user.participant.Participant;
import com.example.healthcoins.domain.user.participant.ParticipantRequest;
import com.example.healthcoins.utils.Constants;

import retrofit2.Call;

public class UserRepository {

    private final HealthCoinAPI webservice;

    public UserRepository() {
        webservice = APIModule.getInstance().provideAPI();
    }

    public LiveData<Resource<Participant>> createParticipant(ParticipantRequest request) {
        return new NetworkBoundResource<Participant, Participant>() {
            @NonNull
            @Override
            protected Call<ApiResponse<Participant>> createCall() {
                return webservice.createParticipant(request);

            }
        }.asLiveData();
    }

    public LiveData<Resource<CareProfessional>> getCareProfessionalByAddress(String careProfessionalAddress, String accessToken) {
        return new NetworkBoundResource<CareProfessional, CareProfessional>() {
            @NonNull
            @Override
            protected Call<ApiResponse<CareProfessional>> createCall() {
                return webservice.getCareProfessionalByAddress(careProfessionalAddress, Constants.AUTHORIZATION_PREFIX + accessToken);
            }
        }.asLiveData();
    }

    public LiveData<Resource<Participant>> getParticipantByAddress(String participantAddress, String accessToken) {
        return new NetworkBoundResource<Participant, Participant>() {
            @NonNull
            @Override
            protected Call<ApiResponse<Participant>> createCall() {
                return webservice.getParticipantByAddress(participantAddress, Constants.AUTHORIZATION_PREFIX + accessToken);
            }
        }.asLiveData();
    }

    public LiveData<Resource<Participant[]>> getParticipants(String accessToken) {
        return new NetworkBoundResource<Participant[], Participant[]>() {
            @NonNull
            @Override
            protected Call<ApiResponse<Participant[]>> createCall() {
                return webservice.getParticipants(Constants.AUTHORIZATION_PREFIX + accessToken);
            }
        }.asLiveData();
    }
}
