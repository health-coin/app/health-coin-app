package com.example.healthcoins.validators;

import android.widget.TextView;

public class TextViewValidator extends WidgetValidator<TextView> {

    public TextViewValidator(InvalidStrategy<TextView> strategy) {
        super(strategy);
    }

    @Override
    boolean validateInput(TextView view) {
        return view.getText() != null && view.getText().length() > 0;
    }
}
