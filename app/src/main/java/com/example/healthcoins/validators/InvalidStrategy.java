package com.example.healthcoins.validators;

public interface InvalidStrategy<T> {

    void onInvalidValidation(T view);

}
