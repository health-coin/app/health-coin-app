package com.example.healthcoins.validators;

import android.widget.TextView;

public class NumberViewValidator extends WidgetValidator<TextView> {

    public NumberViewValidator(InvalidStrategy<TextView> strategy) {
        super(strategy);
    }

    @Override
    boolean validateInput(TextView view) {
        CharSequence text = view.getText();
        return text != null && text.length() > 0 && isInt(text.toString());
    }

    private boolean isInt(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
