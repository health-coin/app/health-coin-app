package com.example.healthcoins.validators;

import android.widget.Spinner;

public class SpinnerValidator extends WidgetValidator<Spinner> {

    public SpinnerValidator(InvalidStrategy<Spinner> strategy) {
        super(strategy);
    }

    @Override
    boolean validateInput(Spinner view) {
        return view.getSelectedItem() != null;
    }
}
