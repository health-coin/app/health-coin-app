package com.example.healthcoins.validators;

public abstract class WidgetValidator<T> {

    private InvalidStrategy<T> strategy;

    public WidgetValidator(InvalidStrategy<T> strategy) {
        this.strategy = strategy;
    }

    public boolean validate(T view) {
        if (!validateInput(view)) {
            strategy.onInvalidValidation(view);
            return false;
        }
        return true;
    }

    abstract boolean validateInput(T view);
}
