package com.example.healthcoins.pages.home.participant;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.domain.treatment.TreatmentRecord;
import com.example.healthcoins.domain.treatment.TreatmentState;
import com.example.healthcoins.repository.TreatmentRepository;

public class MainViewModel extends ViewModel {

    private LiveData<Resource<TreatmentDetail>> treatmentData;
    private TreatmentRepository treatmentRepository = new TreatmentRepository();


    LiveData<Resource<TreatmentDetail>> getActiveTreatment(String accessToken) {
        if (treatmentData != null) {
            return treatmentData;
        }
        this.treatmentData = treatmentRepository.getActiveTreatment(accessToken);
        return treatmentData;
    }

    LiveData<Resource<TreatmentRecord[]>> getDoneTreatments(String accessToken) {
        return treatmentRepository.getTreatmentPlans(TreatmentState.DONE.name(), accessToken);
    }
}
