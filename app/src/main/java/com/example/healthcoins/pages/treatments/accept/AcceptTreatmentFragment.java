package com.example.healthcoins.pages.treatments.accept;

import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.AcceptTreatmentFragmentBinding;
import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.domain.user.careprofessional.CareProfessional;
import com.example.healthcoins.pages.UserViewModel;
import com.example.healthcoins.pages.home.participant.MainView;
import com.example.healthcoins.pages.treatments.GoalListAdapter;
import com.example.healthcoins.repository.TreatmentRepository;
import com.example.healthcoins.services.TokenService;

import java.util.Arrays;

import es.dmoral.toasty.Toasty;

public class AcceptTreatmentFragment extends Fragment {

    private final TreatmentRepository treatmentRepo = new TreatmentRepository();
    private UserViewModel userViewModel;
    private AcceptTreatmentBindingModel acceptTreatmentBindingModel;
    public TreatmentDetail treatmentDetail;
    private NavController navController;
    private String accessToken;
    private AcceptTreatmentFragmentBinding binding;

    public static AcceptTreatmentFragment newInstance() {
        return new AcceptTreatmentFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        acceptTreatmentBindingModel = new AcceptTreatmentBindingModel();
        acceptTreatmentBindingModel.setAcceptedTreatment(false);
        accessToken = TokenService.getInstance(this.getActivity()).getToken().getAccessToken();

        binding = DataBindingUtil.inflate(
                inflater, R.layout.accept_treatment_fragment, container, false);
        View view = binding.getRoot();

        ListView goalMeasurementListView = view.findViewById(R.id.accept_tr_goallist);
        treatmentDetail = (TreatmentDetail) getArguments().getSerializable(MainView.BUNDLE_KEY_FOR_NAV);
        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

        binding.setTreatmentDetail(treatmentDetail);
        binding.setViewModel(acceptTreatmentBindingModel);
        binding.setCallback(this);
        GoalListAdapter adapter = new GoalListAdapter(getActivity(), Arrays.asList(treatmentDetail.getGoalMeasurement()));
        goalMeasurementListView.setAdapter(adapter);
        loadCareProfessionalViewModel();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    public void treatmentAccepted(View view) {
        String id = treatmentDetail.getTreatmentData().getId();

        if(acceptTreatmentBindingModel.getAcceptedTreatment()) {
            ProgressDialog dialog = ProgressDialog.show(getActivity(), "Laden",
                    "Behandelplan accepteren...", true);
            this.treatmentRepo.acceptTreatment(id, accessToken).observe(this, (response) -> {
                if(response.code == null) {
                    Toasty.success(getActivity(), response.message, Toast.LENGTH_LONG).show();
                    navController.navigate(R.id.mainView);
                } else {
                    Toasty.error(getActivity(), response.message, Toast.LENGTH_LONG).show();
                }
                dialog.hide();
            });
        } else {
            Toasty.error(getActivity(), "Je moet akkoord gaan met het behandelplan", Toast.LENGTH_LONG).show();
        }
    }

    private void loadCareProfessionalViewModel(){
        acceptTreatmentBindingModel.setLoadingCareProfessional(true);
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        String careProfessionalAddress = treatmentDetail.getContractData().getCareTaker();
        userViewModel.getCareProfessionalData(careProfessionalAddress, accessToken).observe(this, this::handleCareProfessionalResponse);
    }

    private void handleCareProfessionalResponse(Resource<CareProfessional> careProfessionalResource) {
        acceptTreatmentBindingModel.setLoadingCareProfessional(false);
        if(careProfessionalResource.data != null) {
            binding.setCareProfessional(careProfessionalResource.data);
        }
    }
}
