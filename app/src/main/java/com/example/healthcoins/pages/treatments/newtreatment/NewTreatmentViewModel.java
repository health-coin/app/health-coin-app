package com.example.healthcoins.pages.treatments.newtreatment;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;
import com.example.healthcoins.domain.user.participant.Participant;
import com.example.healthcoins.repository.UserRepository;

public class NewTreatmentViewModel extends ViewModel {
    private final MutableLiveData<MeasurementValue> selectedMeasurement = new MutableLiveData<>();
    private UserRepository repository = new UserRepository();

    public MutableLiveData<MeasurementValue> getSelectedMeasurement() {
        return selectedMeasurement;
    }
    public void setSelectedMeasurement(MeasurementValue measurementValue) {
        this.selectedMeasurement.setValue(measurementValue);
    }

    public LiveData<Resource<Participant[]>> getParticipants(String accessToken) {
        return repository.getParticipants(accessToken);
    }


}
