package com.example.healthcoins.pages.treatments;

import android.content.Context;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;

import com.example.healthcoins.R;

public class TreatmentStateButton extends AppCompatImageButton {

    private static final int[] STATE_DONE = {R.attr.state_treatmentstep_done};
    private boolean done = false;

    public TreatmentStateButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public int[] onCreateDrawableState(int extraSpace) {
        int[] state = super.onCreateDrawableState(extraSpace + 1);
        if (done) {
            mergeDrawableStates(state, STATE_DONE);
        }
        return state;
    }

    public void setDone(boolean done) {
        this.done = done;
        refreshDrawableState();
    }
}
