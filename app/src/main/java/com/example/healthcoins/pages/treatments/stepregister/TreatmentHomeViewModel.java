package com.example.healthcoins.pages.treatments.stepregister;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.healthcoins.domain.treatment.measurement.Measurement;

import java.util.List;

/**
 * ViewModel abstraction over Repository to support live data for UI.
 */
public class TreatmentHomeViewModel extends ViewModel {

    private final MutableLiveData<List<Measurement>> selectMeasurements = new MutableLiveData<>();

    public MutableLiveData<List<Measurement>> getSelectMeasurements() {
        return selectMeasurements;
    }

    public void setSelectMeasurements(List<Measurement> measurements) {
        selectMeasurements.setValue(measurements);
    }
}
