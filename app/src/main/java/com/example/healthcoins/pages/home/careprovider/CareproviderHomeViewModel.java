package com.example.healthcoins.pages.home.careprovider;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.treatment.TreatmentRecord;
import com.example.healthcoins.domain.treatment.TreatmentState;
import com.example.healthcoins.repository.TreatmentRepository;

public class CareproviderHomeViewModel extends ViewModel {

    private String bearerToken;
    private TreatmentRepository treatmentRepository = new TreatmentRepository();

    public void init(String bearerToken) {
        this.bearerToken = bearerToken;
    }

    public LiveData<Resource<TreatmentRecord[]>> getPendingTreatments() {
        return treatmentRepository.getTreatmentPlans(TreatmentState.PENDING.name(), bearerToken);
    }

    LiveData<Resource<TreatmentRecord[]>> getStartedTreatments() {
        return treatmentRepository.getTreatmentPlans(TreatmentState.STARTED.name(), bearerToken);
    }

    LiveData<Resource<TreatmentRecord[]>> getSignedTreatments() {
        return treatmentRepository.getTreatmentPlans(TreatmentState.SIGNED.name(), bearerToken);
    }

}
