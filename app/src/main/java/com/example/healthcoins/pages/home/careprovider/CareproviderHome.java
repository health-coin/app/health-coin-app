package com.example.healthcoins.pages.home.careprovider;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.CareproviderHomeFragmentBinding;
import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.treatment.TreatmentRecord;
import com.example.healthcoins.pages.home.TreatmentListAdapter;
import com.example.healthcoins.pages.home.TreatmentListCallback;
import com.example.healthcoins.pages.treatments.start.StartTreatmentViewModel;
import com.example.healthcoins.services.TokenService;

import java.util.Arrays;

import es.dmoral.toasty.Toasty;

public class CareproviderHome extends Fragment implements TreatmentListCallback {

    private CareproviderHomeViewModel viewModel;
    private CareProviderHomeBindingModel bindingModel;
    private CareproviderHomeFragmentBinding layoutBinding;
    private NavController controller;

    public static CareproviderHome newInstance() {
        return new CareproviderHome();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        layoutBinding = DataBindingUtil.inflate(
                inflater, R.layout.careprovider_home_fragment, container, false);

        controller = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        viewModel = ViewModelProviders.of(this).get(CareproviderHomeViewModel.class);
        bindingModel = new CareProviderHomeBindingModel();

        String accessToken = TokenService.getInstance(getActivity()).getToken().getAccessToken();

        viewModel.init(accessToken);
        layoutBinding.setModel(bindingModel);
        layoutBinding.setCallback(this);

        loadPendingTreatments();
        loadStartedTreatments();
        loadSignedTreatments();

        return layoutBinding.getRoot();
    }

    void loadPendingTreatments() {
        viewModel.getPendingTreatments().observe(this, treatments -> {
            bindingModel.setLoadingPendingTreatments(false);
            ListView pendingListview = getView().findViewById(R.id.careprovider_pending_treatmentlist);
            this.handleTreatmentData(treatments, pendingListview);
            if(treatments.data != null){
                layoutBinding.setPendingTreatments(treatments.data);
            }

        });
    }

    void loadStartedTreatments() {
        viewModel.getStartedTreatments().observe(this, treatments -> {
            bindingModel.setLoadingStartedTreatments(false);
            ListView startedListview = getView().findViewById(R.id.careprovider_started_treatmentlist);
            this.handleTreatmentData(treatments, startedListview);
            if(treatments.data != null) {
                layoutBinding.setStartedTreatments(treatments.data);
            }
        });
    }

    void loadSignedTreatments() {
        viewModel.getSignedTreatments().observe(this, treatments -> {
            ListView signedListview = getView().findViewById(R.id.careprovider_signed_treatmentlist);
            bindingModel.setLoadingSignedTreatments(false);
            this.handleTreatmentData(treatments, signedListview);
            if(treatments.data != null) {
                layoutBinding.setSignedTreatments(treatments.data);
            }
        });

    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private void handleTreatmentData(Resource<TreatmentRecord[]> treatments, ListView listView) {
        if(treatments.data != null) {
            TreatmentListAdapter adapter = new TreatmentListAdapter(getActivity(), Arrays.asList(treatments.data), this);
            listView.setAdapter(adapter);

        } else {
            Toasty.error(getActivity(), treatments.message, Toast.LENGTH_LONG).show();
        }
    }

    public void newTreatmentClicked(View view) {
        controller.navigate(R.id.newTreatmentFragment);
    }

    @Override
    public void onSignedTreatmentClicked(TreatmentRecord record) {
        StartTreatmentViewModel startTreatmentViewModel = ViewModelProviders.of(this.getActivity()).get(StartTreatmentViewModel.class);
        startTreatmentViewModel.setSelectedTreatmentRecord(record);
        controller.navigate(R.id.startTreatmentFragment);
    }

    @Override
    public void onStartedTreatmentClicked(TreatmentRecord record) {
        Bundle bundle = new Bundle();
        bundle.putString("treatmentId", record.getTreatment().getId());
        controller.navigate(R.id.endMeasurementFragment, bundle);
    }

    @Override
    public void onDoneTreatmentClicked(TreatmentRecord record) {

    }
}
