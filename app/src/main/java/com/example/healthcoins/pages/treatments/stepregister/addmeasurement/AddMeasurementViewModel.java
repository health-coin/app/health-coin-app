package com.example.healthcoins.pages.treatments.stepregister.addmeasurement;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;
import com.example.healthcoins.repository.TreatmentRepository;

public class AddMeasurementViewModel extends ViewModel {
    private TreatmentRepository treatmentRepository = new TreatmentRepository();

    public LiveData<Resource<TreatmentDetail>> getTreatmentPlanById(String treatmentId, String accessToken) {
        return treatmentRepository.getTreatmentPlanById(treatmentId, accessToken);
    }

    public LiveData<Resource<Object>> createTreatmentMeasurement(MeasurementValue[] measurementValues, String treatmentId, String accessToken) {
        return treatmentRepository.createMeasurement(measurementValues, treatmentId, accessToken);
    }
}
