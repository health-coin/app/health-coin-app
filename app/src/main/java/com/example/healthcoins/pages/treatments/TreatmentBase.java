package com.example.healthcoins.pages.treatments;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.TreatmentBaseFragmentBinding;
import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.domain.treatment.measurement.Measurement;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;
import com.example.healthcoins.services.TokenService;
import com.example.healthcoins.utils.Constants;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class TreatmentBase extends Fragment {

   private TreatmentBaseViewModel baseViewModel;
    private TreatmentBaseFragmentBinding binding;
    private String accessToken;
    private NavController navController;
    private Measurement lastMeasurement;
    
    public static TreatmentBase newInstance() {
        return new TreatmentBase();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.treatment_base_fragment, container, false);
        View rootView = binding.getRoot();

        View fragmentContainer = rootView.findViewById(R.id.nav_host_treatment);
        navController = Navigation.findNavController(fragmentContainer);
        accessToken = TokenService.getInstance(getActivity()).getToken().getAccessToken();

        initializeViewModel();
        loadActiveTreatment();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private Measurement determineIfFinalMeasurement() {
        TreatmentDetail treatmentDetail = binding.getTreatmentDetail();

        for (Measurement customMeasurement : treatmentDetail.getTreatmentData().getCustomMeasurements()) {
            List<Boolean> checks = new ArrayList<>();

            for (MeasurementValue goalMeasurement : treatmentDetail.getGoalMeasurement()) {
                customMeasurement.getValues().forEach(measurementValue -> {
                    if(goalMeasurement.equals(measurementValue)) {
                        checks.add(true);
                    }
                });

            }
            Log.d("Test", checks.size()+"");
            Log.d("Test", customMeasurement.getValues().size() + "");
            if(checks.size() == customMeasurement.getValues().size()) {
                lastMeasurement = customMeasurement;
                return customMeasurement;
            }
        }
        return null;
    }

    private void handleTreatmentData(Resource<TreatmentDetail> treatmentDetailResource) {
        if(treatmentDetailResource.data == null) {
            Toasty.error(getActivity(), treatmentDetailResource.message, Toasty.LENGTH_LONG).show();
            return;
        }
        Toasty.success(getActivity(), treatmentDetailResource.message, Toasty.LENGTH_LONG).show();
        baseViewModel.setSelectedTreatmentDetail(treatmentDetailResource.data);
        binding.setTreatmentDetail(treatmentDetailResource.data);
        binding.setCallback(this);
        setButtonStates();
    }

    private void loadActiveTreatment() {
        if(getArguments() != null) {
            String treatmentId = getArguments().getString("treatmentId");
            if(treatmentId != null) {
                baseViewModel.getTreatmentPlanById(treatmentId, accessToken).observe(this, this::handleTreatmentData);
            }
        } else {
            baseViewModel.getActiveTreatment(accessToken).observe(this, this::handleTreatmentData);
        }
    }

    private void initializeViewModel() {
        baseViewModel = ViewModelProviders.of(getActivity()).get(TreatmentBaseViewModel.class);
    }

    private void setButtonStates() {
        Measurement finalMeasurement = determineIfFinalMeasurement();

        if(binding.getTreatmentDetail().getContractData().isStarted()) {
            setStepDisabled(binding.thrStep1, false);
            setStepDisabled(binding.thrStep2, true);
            setStepDisabled(binding.thrStep3, true);
            setStepDisabled(binding.thrStep4, true);

            if(finalMeasurement != null) {
                this.onMeasurementClicked(null);
            }
        }
        if(binding.getTreatmentDetail().getContractData().isEndMeasurementAdded()) {
            setStepDone(binding.thrStep1, true);
            setStepDisabled(binding.thrStep2, true);
            setStepDone(binding.thrStep3, false);
            setStepDisabled(binding.thrStep3, false);
            this.onSignClicked(null);
        }
        if(binding.getTreatmentDetail().getContractData().isDone()) {
            setStepDone(binding.thrStep1, true);
            setStepDisabled(binding.thrStep2, true);
            setStepDone(binding.thrStep3, true);
            setStepDone(binding.thrStep4, true);
            setStepDisabled(binding.thrStep4, false);
            this.onDoneClicked(null);
        }
        if(finalMeasurement != null) {
            setStepDone(binding.thrStep1, true);
            setStepDisabled(binding.thrStep2, false);
            setStepDone(binding.thrStep2, true);
        }
    }

    private void setStepDone(TreatmentStateButton button, boolean state) {
        button.setDone(state);
    }

    private void setStepDisabled(TreatmentStateButton button, boolean state) {
        button.setEnabled(!state);
    }

    public void onStartClicked(View view) {
        navController.navigate(R.id.treatmentHome);

    }
    public void onMeasurementClicked(View view) {
        loadWithCustomFinalResult(R.id.treatmentMeasure);
    }

    public void onSignClicked(View view) {
        loadWithCustomFinalResult(R.id.stepSignAfterEndMeasurementFragment);
    }

    public void onDoneClicked(View view) {
        loadWithCustomFinalResult(R.id.treatmentDone);
    }

    private void loadWithCustomFinalResult(int resId) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.TREATMENT_BASE_TO_MEASUREMENT_KEY, lastMeasurement);
        navController.navigate(resId, bundle);
    }
}
