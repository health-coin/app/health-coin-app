package com.example.healthcoins.pages.treatments.start;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.domain.treatment.TreatmentRecord;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;
import com.example.healthcoins.repository.TreatmentRepository;

public class StartTreatmentViewModel extends ViewModel {
    private TreatmentRepository treatmentRepository = new TreatmentRepository();
    private final MutableLiveData<TreatmentRecord> selectedTreatmentRecord = new MutableLiveData<>();

    public LiveData<Resource<TreatmentDetail>> getTreatmentById(String id, String accessToken) {
        return treatmentRepository.getTreatmentPlanById(id, accessToken);
    }

    public MutableLiveData<TreatmentRecord> getSelectedTreatmentRecord() {
        return selectedTreatmentRecord;
    }

    public void setSelectedTreatmentRecord(TreatmentRecord detail) {
        selectedTreatmentRecord.setValue(detail);
    }

    public LiveData<Resource<Object>> createTreatmentMeasurement(MeasurementValue[] measurementValues, String treatmentId, String accessToken) {
        return treatmentRepository.createMeasurement(measurementValues, treatmentId, accessToken);
    }
}
