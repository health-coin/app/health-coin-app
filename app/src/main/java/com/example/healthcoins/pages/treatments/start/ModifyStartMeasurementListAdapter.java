package com.example.healthcoins.pages.treatments.start;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.TreatmentStartmeasurementListrowBinding;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;

import java.util.List;

public class ModifyStartMeasurementListAdapter extends BaseAdapter {
    private List<MeasurementValue> measurementList;
    private Context activity;

    public ModifyStartMeasurementListAdapter(
            Context activity,
            List<MeasurementValue> measurementList) {
        this.measurementList = measurementList;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return measurementList.size();
    }

    @Override
    public Object getItem(int position) {
        return measurementList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TreatmentStartmeasurementListrowBinding binding;
        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.treatment_startmeasurement_listrow, null);
            binding = DataBindingUtil.bind(convertView);
            convertView.setTag(binding);
        } else {
            binding = (TreatmentStartmeasurementListrowBinding) convertView.getTag();
        }
        binding.setStartMeasurementValue(measurementList.get(position));
        return binding.getRoot();
    }
}
