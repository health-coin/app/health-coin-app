package com.example.healthcoins.pages.treatments.accept;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.android.databinding.library.baseAdapters.BR;

public class AcceptTreatmentBindingModel extends BaseObservable {

    private boolean acceptedTreatment = false;
    private boolean isLoadingCareProfessional = true;
    @Bindable
    public Boolean getAcceptedTreatment() {
        return acceptedTreatment;
    }

    public void setAcceptedTreatment(Boolean value) {
        // Avoids infinite loops.
        if (acceptedTreatment!= value) {
            acceptedTreatment = value;
            notifyPropertyChanged(BR.acceptedTreatment);
        }
    }

    @Bindable
    public boolean getIsLoadingCareProfessional() {
        return isLoadingCareProfessional;
    }

    public void setLoadingCareProfessional(boolean loadingCareProfessional) {
        isLoadingCareProfessional = loadingCareProfessional;
        notifyPropertyChanged(BR.isLoadingCareProfessional);

    }
}