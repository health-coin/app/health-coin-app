package com.example.healthcoins.pages.treatments.newtreatment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.example.healthcoins.R;
import com.example.healthcoins.databinding.TreatmentGoalModifyingListrowBinding;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;
import java.util.List;

public class GoalListModifyingAdapter extends BaseAdapter implements GoalListModifyingCallback {

    private List<MeasurementValue> measurementList;
    private Context activity;
    private GoalListModifyingCallback clickListener;

    public GoalListModifyingAdapter(
            Context activity,
            List<MeasurementValue> measurementList,
            GoalListModifyingCallback clickListener) {
        this.measurementList = measurementList;
        this.activity = activity;
        this.clickListener = clickListener;
    }

    @Override
    public int getCount() {
        return measurementList.size();
    }

    @Override
    public Object getItem(int position) {
        return measurementList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TreatmentGoalModifyingListrowBinding binding;
        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.treatment_goal_modifying_listrow, null);
            binding = DataBindingUtil.bind(convertView);
            convertView.setTag(binding);
        } else {
            binding = (TreatmentGoalModifyingListrowBinding) convertView.getTag();
        }
        binding.setGoalMeasurementValue(measurementList.get(position));
        binding.setCallback(this);
        return binding.getRoot();
    }

    @Override
    public void onDeleteClicked(View view, MeasurementValue measurementValue) {
        clickListener.onDeleteClicked(view, measurementValue);
        notifyDataSetChanged();
    }

    @Override
    public void onEditClicked(View view, MeasurementValue measurementValue) {
        clickListener.onEditClicked(view, measurementValue);
    }
}
