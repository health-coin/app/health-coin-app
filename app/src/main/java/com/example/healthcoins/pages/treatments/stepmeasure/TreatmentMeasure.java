package com.example.healthcoins.pages.treatments.stepmeasure;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.TreatmentMeasureFragmentBinding;
import com.example.healthcoins.domain.treatment.measurement.Measurement;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;
import com.example.healthcoins.pages.treatments.GoalListAdapter;
import com.example.healthcoins.pages.treatments.TreatmentBaseViewModel;
import com.example.healthcoins.pages.treatments.TreatmentListViewAdapter;
import com.example.healthcoins.pages.treatments.TreatmentListViewValue;
import com.example.healthcoins.services.TokenService;
import com.example.healthcoins.utils.Constants;
import com.example.healthcoins.utils.DateUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class TreatmentMeasure extends Fragment {

    private TreatmentBaseViewModel treatmentBaseViewModel;
    private TreatmentMeasureFragmentBinding binding;
    private Measurement finalCustomMeasurement;

    public static TreatmentMeasure newInstance() {
        return new TreatmentMeasure();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        initializeViewModel();
        binding = DataBindingUtil.inflate(inflater, R.layout.treatment_measure_fragment, container, false);
        this.finalCustomMeasurement = (Measurement) getArguments().getSerializable(Constants.TREATMENT_BASE_TO_MEASUREMENT_KEY);
        loadTreatmentDetail();
        return binding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void initializeViewModel() {
        treatmentBaseViewModel = ViewModelProviders.of(getActivity()).get(TreatmentBaseViewModel.class);
    }

    private void loadTreatmentDetail() {
        binding.setIsLoading(true);
        treatmentBaseViewModel.getSelectedTreatmentDetail().observe(getActivity(), treatmentDetail -> {
            binding.setTreatmentDetail(treatmentDetail);
            binding.treatmentCustomMeasurementInfo.setText("Volgens jouw eigen metingen heb je behandelplan " + treatmentDetail.getTreatmentData().getId() + " gehaald!");
            binding.setIsLoading(false);
            if(checkLastMeasurement()) {
                setFinalResults();
                setBehandelplanList();

            }
        });

    }

    private boolean checkLastMeasurement() {
        if(finalCustomMeasurement == null) {
            Toasty.error(getActivity(), "Je hebt geen eigen metingen", Toasty.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void setFinalResults() {
        ListView latestMeasurementsView = binding.archievedResultList;
        Measurement latestMeasurement = finalCustomMeasurement;
        TreatmentListViewAdapter listViewAdapter = new GoalListAdapter(getActivity(), latestMeasurement.getValues());
        latestMeasurementsView.setAdapter(listViewAdapter);
    }

    private void setBehandelplanList() {
        ListView behandelPlanList = binding.startTreatmentGoallist;
        List<MeasurementValue> measurementValueList = Arrays.asList(binding.getTreatmentDetail().getGoalMeasurement());
        setValuesAndAdapter(behandelPlanList, measurementValueList);
    }

    private void setValuesAndAdapter(ListView behandelPlanList, List<MeasurementValue> measurementValueList) {
        Measurement latestMeasurement = finalCustomMeasurement;
        TreatmentListViewAdapter listAdapter = new GoalListAdapter(getActivity(), measurementValueList);
        TreatmentListViewValue endDate = new TreatmentListViewValue("Eind datum", "=", DateUtils.formatDateOnYear(latestMeasurement.getDate()));
        TreatmentListViewValue startDate = new TreatmentListViewValue("Start datum", "=", DateUtils.formatDateOnYear(new Date(binding.getTreatmentDetail().getContractData().getStartDate())));
        TreatmentListViewValue planned = new TreatmentListViewValue("Gepland", "=", DateUtils.formatDateOnYear(new Date(binding.getTreatmentDetail().getContractData().getPlannedEndDate())));
        TreatmentListViewValue inProgress = new TreatmentListViewValue("Status", "=", "In afwachting");

        listAdapter.addValue(inProgress);
        listAdapter.addValue(startDate);
        listAdapter.addValue(endDate);
        listAdapter.addValue(planned);
        behandelPlanList.setAdapter(listAdapter);
    }

}
