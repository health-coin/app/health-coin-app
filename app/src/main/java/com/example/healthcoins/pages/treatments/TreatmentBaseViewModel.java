package com.example.healthcoins.pages.treatments;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.domain.treatment.measurement.Measurement;
import com.example.healthcoins.repository.TreatmentRepository;

public class TreatmentBaseViewModel extends ViewModel {
    private final MutableLiveData<TreatmentDetail> selectedTreatmentDetail = new MutableLiveData<>();

    private TreatmentRepository treatmentRepository = new TreatmentRepository();

    public TreatmentBaseViewModel() {

    }

    public LiveData<Resource<TreatmentDetail>> getTreatmentPlanById(String treatmentId, String accessToken) {
        return treatmentRepository.getTreatmentPlanById(treatmentId, accessToken);
    }

    public LiveData<Resource<TreatmentDetail>> getActiveTreatment(String accessToken) {
        return treatmentRepository.getActiveTreatment(accessToken);
    }

    public MutableLiveData<TreatmentDetail> getSelectedTreatmentDetail() {
        return selectedTreatmentDetail;
    }

    public void setSelectedTreatmentDetail(TreatmentDetail detail) {
        selectedTreatmentDetail.setValue(detail);
    }
}
