package com.example.healthcoins.pages.treatments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.TreatmentGoalListrowBinding;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;
import com.example.healthcoins.pages.treatments.TreatmentListViewAdapter;
import com.example.healthcoins.pages.treatments.TreatmentListViewValue;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GoalListAdapter extends TreatmentListViewAdapter {

    public GoalListAdapter(Context activity, List<MeasurementValue> measurementValues) {
        super(activity, createListViewValues(measurementValues));
    }

    private static List<TreatmentListViewValue> createListViewValues(List<MeasurementValue> measurementValues) {
        List<TreatmentListViewValue> treatmentValues = new ArrayList<>();
        for (MeasurementValue value : measurementValues) {
            treatmentValues.add(new TreatmentListViewValue(value.getUnit(), value.getComparisonText(), value.getQuantity() + ""));
        }
        return treatmentValues;
    }
}
