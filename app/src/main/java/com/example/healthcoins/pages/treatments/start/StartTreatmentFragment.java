package com.example.healthcoins.pages.treatments.start;

import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import com.example.healthcoins.R;
import com.example.healthcoins.databinding.StartTreatmentFragmentBinding;
import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;
import com.example.healthcoins.domain.user.participant.Participant;
import com.example.healthcoins.pages.UserViewModel;
import com.example.healthcoins.pages.treatments.GoalListAdapter;
import com.example.healthcoins.services.TokenService;

import java.util.Arrays;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class StartTreatmentFragment extends Fragment {

    private StartTreatmentViewModel viewModel;
    private UserViewModel userViewModel;
    private StartTreatmentBindingModel bindingModel;
    private String bearerToken;
    private StartTreatmentFragmentBinding binding;
    private NavController navController;

    public static StartTreatmentFragment newInstance() {
        return new StartTreatmentFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.start_treatment_fragment, container, false);
        bindingModel = new StartTreatmentBindingModel();
        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        bearerToken = TokenService.getInstance(getActivity()).getToken().getAccessToken();

        binding.setModel(bindingModel);
        binding.setCallback(this);
        loadViewModel();
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void loadUserViewModel() {
        userViewModel = ViewModelProviders.of(getActivity()).get(UserViewModel.class);
        String participantAddress = binding.getModel().getTreatmentDetail().getContractData().getParticipant();
        userViewModel.getParticipantData(participantAddress, bearerToken).observe(this, this::handleParticipantResponse);
    }

    private void loadViewModel() {
        bindingModel.setTreatmentIsLoading(true);
        viewModel = ViewModelProviders.of(getActivity()).get(StartTreatmentViewModel.class);

        viewModel.getSelectedTreatmentRecord().observe(this, treatmentRecord -> {
            if(treatmentRecord != null) {
                viewModel.getTreatmentById(treatmentRecord.getTreatment().getId(), bearerToken).observe(this, this::handleTreatmentResponse);

            } else {
                Toasty.error(getActivity(), "Behandeling is niet geselecteerd.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void handleParticipantResponse(Resource<Participant> participantResource) {
        if(!hasAndShowError(participantResource.code, participantResource.message)) {
            bindingModel.setParticipant(participantResource.data);
        }
        bindingModel.setParticipantIsLoading(false);
    }

    private void handleTreatmentResponse(Resource<TreatmentDetail> treatmentDetailResource) {
        if(!hasAndShowError(treatmentDetailResource.code, treatmentDetailResource.message)) {
            bindingModel.setTreatmentDetail(treatmentDetailResource.data);
            loadGoalAdapter();
            loadStartMeasurementFieldAdapter();
            loadUserViewModel();

        }
        bindingModel.setTreatmentIsLoading(false);
    }

    private boolean hasAndShowError(String code, String message) {
        boolean hasError = code != null;
        if(code != null) {
            Toasty.error(getActivity(), message, Toast.LENGTH_LONG).show();
        } else {
            Toasty.success(getActivity(), message, Toast.LENGTH_LONG).show();
        }
        return hasError;
    }

    private void loadStartMeasurementFieldAdapter() {
        bindingModel.getTreatmentDetail().setStartMeasurement(bindingModel.getTreatmentDetail().getGoalMeasurement().clone());
        List<MeasurementValue> measurementValueList = Arrays.asList(bindingModel.getTreatmentDetail().getStartMeasurement());
        ModifyStartMeasurementListAdapter modifyStartMeasurementListAdapter = new ModifyStartMeasurementListAdapter(getActivity(), measurementValueList);
        binding.startMeasurementEditquantities.setAdapter(modifyStartMeasurementListAdapter);
    }

    private void loadGoalAdapter(){
        List<MeasurementValue> measurementValueList = Arrays.asList(this.bindingModel.getTreatmentDetail().getGoalMeasurement());
        GoalListAdapter goalListAdapter = new GoalListAdapter(getActivity(), measurementValueList);
        binding.startTreatmentGoallist.setAdapter(goalListAdapter);
    }

    public void addStartMeasurement(View view) {
        MeasurementValue[] startMeasurements = bindingModel.getTreatmentDetail().getStartMeasurement();
        String treatmentId = bindingModel.getTreatmentDetail().getTreatmentData().getId();
        ProgressDialog dialog = ProgressDialog.show(getActivity(), "Laden",
                "Start meting toevoegen...", true);
        viewModel.createTreatmentMeasurement(startMeasurements, treatmentId, bearerToken).observe(this, objectResource -> {
            if(!hasAndShowError(objectResource.code, objectResource.message)) {
                navController.popBackStack();
            }
            dialog.hide();
        });
    }
}
