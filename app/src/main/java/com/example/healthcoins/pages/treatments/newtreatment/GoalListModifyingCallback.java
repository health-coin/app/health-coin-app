package com.example.healthcoins.pages.treatments.newtreatment;

import android.view.View;

import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;

public interface GoalListModifyingCallback {
    void onDeleteClicked(View view, MeasurementValue measurementValue);
    void onEditClicked(View view, MeasurementValue measurementValue);
}
