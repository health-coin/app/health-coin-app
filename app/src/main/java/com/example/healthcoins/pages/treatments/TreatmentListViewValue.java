package com.example.healthcoins.pages.treatments;

import android.support.annotation.Nullable;

public class TreatmentListViewValue {

    private final String leftValue;
    private String comparisonText;
    private String rightValue;

    public TreatmentListViewValue(String leftValue) {
        this(leftValue, null, null);
    }

    public TreatmentListViewValue(String leftValue, String comparisonText) {
        this(leftValue, comparisonText, null);
    }

    public TreatmentListViewValue(String leftValue, @Nullable String comparisonText, @Nullable String rightValue) {
        this.leftValue = leftValue;
        this.comparisonText = comparisonText == null ? "=" : comparisonText;
        this.rightValue = rightValue == null ? "" : rightValue;
    }

    public String getLeftValue() {
        return leftValue;
    }

    public String getComparisonText() {
        return comparisonText;
    }

    public String getRightValue() {
        return rightValue;
    }
}
