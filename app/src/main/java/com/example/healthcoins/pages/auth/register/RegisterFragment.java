package com.example.healthcoins.pages.auth.register;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.healthcoins.domain.user.participant.ParticipantRequest;
import com.example.healthcoins.domain.user.PersonData;
import com.example.healthcoins.pages.auth.login.LoginFragment;
import com.example.healthcoins.pages.auth.CustomFragment;
import com.example.healthcoins.R;
import com.example.healthcoins.repository.UserRepository;
import com.example.healthcoins.utils.ValidatorUtils;
import com.example.healthcoins.validators.InvalidStrategy;
import com.example.healthcoins.validators.NumberViewValidator;
import com.example.healthcoins.validators.SpinnerValidator;
import com.example.healthcoins.validators.TextViewValidator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RegisterFragment extends CustomFragment {

    private final UserRepository participantRepo = new UserRepository();
    private Spinner genderSpinner;

    public RegisterFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadActivity();
    }

    private void loadActivity() {
        loadGenderSpinner();
        setRegisterButtonAction();
    }

    private void loadGenderSpinner() {
        genderSpinner = getActivity().findViewById(R.id.genderSpinner);
        new CustomSpinner(genderSpinner, getContext()).loadSpinner();
    }

    private void setRegisterButtonAction() {
        Button registerButton = getActivity().findViewById(R.id.buttonRegister);
        registerButton.setOnClickListener(view -> {
            if (validateInputFields()) {
                participantRepo.createParticipant(createParticipantRequest()).observe(this, result -> {
                    if (result == null) {
                        displayMessage("Registratie waardes zijn invalide");
                        return;
                    }
                    if (result.data == null) {
                        displayMessage(result.message);
                        return;
                    }
                    displayMessage("Account aangemaakt");
                    setFragment(new LoginFragment());
                });
            }
        });
    }

    private boolean validateInputFields() {
        return validateTextInputFields() && validateNumberInputFields() && validateSpinners();
    }

    private boolean validateTextInputFields() {
        InvalidStrategy<TextView> strategy = getInvalidTextViewStrategy();
        TextViewValidator validator = new TextViewValidator(strategy);
        FragmentActivity activity = getActivity();

        List<TextView> views = Arrays.asList(
                activity.findViewById(R.id.emailText),
                activity.findViewById(R.id.passwordText),
                activity.findViewById(R.id.birthdayText),
                activity.findViewById(R.id.personalIdText),
                activity.findViewById(R.id.firstNameText),
                activity.findViewById(R.id.lastNameText),
                activity.findViewById(R.id.policyNumberText)
        );
        return ValidatorUtils.validateTextViewValidations(validator, views);
    }

    private boolean validateNumberInputFields() {
        InvalidStrategy<TextView> strategy = getInvalidTextViewStrategy();
        NumberViewValidator validator = new NumberViewValidator(strategy);
        FragmentActivity activity = getActivity();

        List<TextView> views = Arrays.asList(
                activity.findViewById(R.id.personalIdText),
                activity.findViewById(R.id.policyNumberText)
        );
        return ValidatorUtils.validateNumberViewValidations(validator, views);
    }

    private InvalidStrategy<TextView> getInvalidTextViewStrategy() {
        return view -> displayMessage(view.getHint() + " is invalide.");
    }

    private boolean validateSpinners() {
        InvalidStrategy<Spinner> strategy = spinner -> displayMessage(spinner.getPrompt() + " is invalide.");
        SpinnerValidator validator = new SpinnerValidator(strategy);
        return ValidatorUtils.validateSpinnerValidations(validator, Collections.singletonList(genderSpinner));
    }

    private ParticipantRequest createParticipantRequest() {
        FragmentActivity activity = getActivity();
        TextView emailView = activity.findViewById(R.id.emailText);
        TextView passwordView = activity.findViewById(R.id.passwordText);
        TextView birthdayView = activity.findViewById(R.id.birthdayText);
        TextView personalIdView = activity.findViewById(R.id.personalIdText);
        TextView firstNameView = activity.findViewById(R.id.firstNameText);
        TextView lastNameView = activity.findViewById(R.id.lastNameText);
        TextView policyNumberView = activity.findViewById(R.id.policyNumberText);
        int genderNumber = genderSpinner.getSelectedItemPosition() + 1;

        return new ParticipantRequest(
                emailView.getText().toString(),
                passwordView.getText().toString(),
                Integer.parseInt(personalIdView.getText().toString()),
                new PersonData(
                        "", birthdayView.getText().toString(),
                        firstNameView.getText().toString(),
                        lastNameView.getText().toString(),
                        policyNumberView.getText().toString(),
                        "",
                        Integer.toString(genderNumber),
                        "")
        );
    }

    private void displayMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }
}
