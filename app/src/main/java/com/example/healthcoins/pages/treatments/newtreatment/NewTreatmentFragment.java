package com.example.healthcoins.pages.treatments.newtreatment;

import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.NewTreatmentFragmentBinding;
import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.treatment.CreateTreatmentRequest;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;
import com.example.healthcoins.domain.user.UserData;
import com.example.healthcoins.domain.user.participant.Participant;
import com.example.healthcoins.repository.TreatmentRepository;
import com.example.healthcoins.services.TokenService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class NewTreatmentFragment extends Fragment implements
        SearchView.OnQueryTextListener, ImageView.OnClickListener, ListView.OnItemClickListener, GoalListModifyingCallback {
    private final TreatmentRepository treatmentRepo = new TreatmentRepository();

    private NewTreatmentViewModel viewModel;
    private List<MeasurementValue> measurementGoal = new ArrayList<>();
    private NewTreatmentBindingModel model = new NewTreatmentBindingModel();
    private CreateTreatmentRequest request = new CreateTreatmentRequest();
    private NewTreatmentFragmentBinding binding;
    private ParticipantListAdapter searchResultAdapter;
    private NavController navController;
    private String accessToken;

    public static NewTreatmentFragment newInstance() {
        return new NewTreatmentFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity()).get(NewTreatmentViewModel.class);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        binding = DataBindingUtil.inflate(
                inflater, R.layout.new_treatment_fragment, container, false);
        accessToken = TokenService.getInstance(getActivity()).getToken().getAccessToken();
        setBindings();
        loadParticipantData();
        initializeBindingVariables();
        return binding.getRoot();
    }

    private void setBindings() {
        binding.setModel(model);
        binding.setCallback(this);
        binding.setRequest(request);
        PickedDate date = new PickedDate(this.request);
        binding.setDate(date);
    }

    private void setUpSearchbar(Resource<Participant[]> participants) {
        if(participants.data != null) {
            int searchCloseButtonId = binding.searchForParticipant.getContext().getResources()
                    .getIdentifier("android:id/search_close_btn", null, null);
            ImageView closeButton = binding.searchForParticipant.findViewById(searchCloseButtonId);
            List<Participant> participantList = Arrays.asList(participants.data);
            searchResultAdapter = new ParticipantListAdapter(participantList);

            binding.searchForParticipant.setOnQueryTextListener(this);
            binding.searchQueryResult.setAdapter(searchResultAdapter);
            binding.searchQueryResult.setOnItemClickListener(this);
        } else {
            Toasty.error(getActivity(), participants.message, Toasty.LENGTH_LONG).show();
        }
        model.setLoadingParticipants(false); // normally this should happen after loading all participants

    }

    private void initializeBindingVariables() {
        GoalListModifyingAdapter listAdapter = new GoalListModifyingAdapter(getActivity(), this.measurementGoal, this);
        binding.newTreatmentGoalList.setAdapter(listAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    /**
     * TODO: Needs replacement with participant data
     */
    public void loadParticipantData() {
        viewModel.getParticipants(accessToken).observe(this, this::setUpSearchbar);
    }

    public void createTreatmentPlan(View view) {
        if(model.getSelectedUser() != null) {
            request.setParticipantId(model.getSelectedUser().getId());
        }
        request.setMeasurements(measurementGoal.toArray(new MeasurementValue[0]));
        ProgressDialog dialog = ProgressDialog.show(getActivity(), "Laden",
                "Nieuw behandelplan maken...", true);
        treatmentRepo.createTreatmentPlan(request, accessToken).observe(this, response -> {
            if(response.code != null) {
                Toasty.error(getActivity(), response.message, Toast.LENGTH_SHORT).show();
            } else {
                Toasty.success(getActivity(), response.message, Toast.LENGTH_SHORT).show();
                navController.popBackStack();

            }
            dialog.hide();
        });
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if(!newText.isEmpty()) {
            model.setShowSuggestions(true);
            searchResultAdapter.getFilter().filter(newText);
        }

        return false;
    }

    @Override
    public void onClick(View view) {
        binding.searchForParticipant.setQuery("", false);
        model.setShowSuggestions(false);

    }

    public void navigateToGoalModifyPage(View view) {
        measurementGoal.add(new MeasurementValue("", "BMI", 25));
        viewModel.setSelectedMeasurement(measurementGoal.get(measurementGoal.size()-1));
        navController.navigate(R.id.modifyGoalMeasurementFragment);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        model.setShowSuggestions(false);
        Participant selectedUser = (Participant) adapterView.getAdapter().getItem(i);
        model.setSelectedUser(selectedUser);
    }

    @Override
    public void onDeleteClicked(View view, MeasurementValue measurementValue) {
        measurementGoal.removeIf(x -> x.getUnit().equals(measurementValue.getUnit()));
        Toasty.success(getActivity(), "Succesvol verwijderd", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEditClicked(View view, MeasurementValue measurementValue) {
        viewModel.setSelectedMeasurement(measurementValue);
        navController.navigate(R.id.modifyGoalMeasurementFragment);
    }
}
