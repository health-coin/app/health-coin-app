package com.example.healthcoins.pages.home.participant;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.domain.treatment.TreatmentRecord;
import com.example.healthcoins.domain.treatment.TreatmentState;
import com.example.healthcoins.R;
import com.example.healthcoins.databinding.MainViewFragmentBinding;
import com.example.healthcoins.domain.treatment.contract.TreatmentContract;
import com.example.healthcoins.pages.home.TreatmentListAdapter;
import com.example.healthcoins.pages.home.TreatmentListCallback;
import com.example.healthcoins.services.TokenService;

import java.util.Arrays;

import es.dmoral.toasty.Toasty;

public class MainView extends Fragment implements TreatmentListCallback {
    public static final String BUNDLE_KEY_FOR_NAV = "activeTreatment";
    private MainViewModel mainViewModel;
    private TreatmentDetail treatmentDetail;
    private NavController navController;
    private MainViewFragmentBinding binding;
    private String accessToken;

    public static MainView newInstance() {
        return new MainView();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        binding = DataBindingUtil.inflate(
                inflater, R.layout.main_view_fragment, container, false);
        View view = binding.getRoot();
        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        accessToken = TokenService.getInstance(getActivity()).getToken().getAccessToken();
        binding.setIsLoading(true);
        binding.setIsLoadingDoneTreatments(true);
        binding.setHasError(false);
        loadActiveTreatment();
        loadDoneTreatments();

        return view;
    }

    private void loadDoneTreatments() {
        mainViewModel.getDoneTreatments(accessToken).observe(this, treatmentRecords -> {
            ListView doneListview = binding.doneTreatmentList;
            this.setOnTreatmentDoneClickListener(doneListview);
            this.handleDoneTreatmentListData(treatmentRecords, doneListview);

        });
    }
    private void setOnTreatmentDoneClickListener(ListView doneListview) {
        doneListview.setOnItemClickListener((adapterView, view, index, longValue) -> {
            TreatmentRecord record = (TreatmentRecord) adapterView.getItemAtPosition(index);
            Toasty.success(getActivity(), record.getTreatment().getId(), Toasty.LENGTH_LONG).show();
        });
    }

    private void handleDoneTreatmentListData(Resource<TreatmentRecord[]> treatmentRecords, ListView doneListview) {
        if(treatmentRecords.data != null) {
            TreatmentListAdapter adapter = new TreatmentListAdapter(getActivity(), Arrays.asList(treatmentRecords.data), this);
            doneListview.setAdapter(adapter);

        } else {
            Toasty.error(getActivity(), treatmentRecords.message, Toast.LENGTH_LONG).show();
        }
        binding.setIsLoadingDoneTreatments(false);

    }


    private void loadActiveTreatment() {
        mainViewModel.getActiveTreatment(accessToken).observe(this, treatmentResponse -> {
            if (treatmentResponse.data != null) {
                this.treatmentDetail = treatmentResponse.data;
                binding.setTreatmentDetail(this.treatmentDetail);
                binding.setCallback(this);

            } else {
                binding.setHasError(true);
                Toasty.error(getActivity(), treatmentResponse.message, Toast.LENGTH_LONG).show();
            }

            binding.setIsLoading(false);

        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public boolean treatmentIsPending() {
        return treatmentDetail.getContractData().isPending();
    }

    public boolean treatmentRequiresAction() {
        return treatmentDetail.getContractData().isStarted()
                || treatmentDetail.getContractData().isEndMeasurementAdded()
                || treatmentDetail.getContractData().isDone();
    }

    public boolean treatmentNoActionRequired() {
        TreatmentContract contractData = treatmentDetail.getContractData();
        return contractData.isSigned() || contractData.isDone();
    }

    public String getTreatmentTitle() {
        String state = this.treatmentDetail.getContractData().getState();
        TreatmentState treatmentState = TreatmentState.valueOf(state);
        switch(treatmentState) {
            case PENDING:
                return "Nieuw behandelplan";
            case STARTED:
                return "Huidig behandeplan";
            case END_MEASUREMENT_ADDED:
                return "Wachtend op jouw goedkeuring";
        }
        return "Je hebt geen actief behandelplan";
    }

    public void treatmentAcceptClicked(View view) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(this.BUNDLE_KEY_FOR_NAV, this.treatmentDetail);
        navController.navigate(R.id.acceptTreatmentFragment, bundle);
    }

    public void startedTreatmentClicked(View view) {
        if(treatmentRequiresAction()) {
            navController.navigate(R.id.treatmentBase);
        }
    }


    @Override
    public void onSignedTreatmentClicked(TreatmentRecord record) {

    }

    @Override
    public void onStartedTreatmentClicked(TreatmentRecord record) {

    }

    @Override
    public void onDoneTreatmentClicked(TreatmentRecord record) {
        Bundle bundle = new Bundle();
        bundle.putString("treatmentId", record.getTreatment().getId());
        navController.navigate(R.id.treatmentBase, bundle);

    }
}
