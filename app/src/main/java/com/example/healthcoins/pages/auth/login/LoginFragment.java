package com.example.healthcoins.pages.auth.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.example.healthcoins.MainActivity;
import com.example.healthcoins.pages.auth.register.RegisterFragment;
import com.example.healthcoins.pages.auth.CustomFragment;
import com.example.healthcoins.R;
import com.example.healthcoins.repository.AuthRepository;
import com.example.healthcoins.services.TokenService;
import com.example.healthcoins.services.UserService;

import es.dmoral.toasty.Toasty;

public class LoginFragment extends CustomFragment {

    private final AuthRepository authRepo = new AuthRepository();
    private TextView emailView;
    private TextView passwordView;

    public LoginFragment() {

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        /**
         * Token isn't expired yet, so redirect to home.
         * TODO: Try to handle this inside a splashscreen.
         * TODO: Depending on role: Redirect to participant home or careprovider home.
         */
        if(!TokenService.getInstance(getActivity()).isExpired()) {
            startMainActivity();
            Toasty.success(getActivity(), "Je bent al ingelogd!", Toast.LENGTH_LONG).show();
        }

        emailView = getView().findViewById(R.id.emailText);
        passwordView = getView().findViewById(R.id.passwordText);
        TextView registerView = getView().findViewById(R.id.orRegisterText);

        Button loginButton = getView().findViewById(R.id.loginButton);
        loginButton.setOnClickListener(view -> {
            String email = emailView.getText().toString();
            String password = passwordView.getText().toString();

            authRepo.loginUser(email, password).observe(this, loginData -> {
                if (loginData == null) {
                    Toasty.success(getActivity(), "Geen token gevonden", Toast.LENGTH_LONG).show();
                    return;
                }

                if (loginData.data != null) {
                    TokenService.getInstance(getActivity()).setToken(loginData.data.getToken());
                    UserService.getInstance(getActivity()).setUser(loginData.data);
                    startMainActivity();
                } else {
                    Toasty.error(getActivity(), loginData.message, Toast.LENGTH_LONG).show();
                }
            });
        });

        registerView.setOnClickListener(view -> setFragment(new RegisterFragment()));
    }

    private void startMainActivity() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
    }
}
