package com.example.healthcoins.pages.wallet;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.transaction.BalanceData;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.repository.BalanceRepository;
import com.example.healthcoins.repository.TreatmentRepository;

public class WalletHomeViewModel extends ViewModel {

    private LiveData<Resource<BalanceData>> balanceData;
    private LiveData<Resource<TreatmentDetail>> activeTreatment;
    private BalanceRepository balanceRepository = new BalanceRepository();
    private TreatmentRepository treatmentRepository = new TreatmentRepository();

    public WalletHomeViewModel() {

    }

    public void init(String accessToken) {

    }

    public LiveData<Resource<BalanceData>> getBalance(String accessToken)
    {
        if (balanceData != null) {
            return balanceData;
        }
        balanceData = balanceRepository.getBalance(accessToken);
        return balanceData;
    }


    public LiveData<Resource<TreatmentDetail>> getActiveTreatment(String accessToken)
    {
        if (activeTreatment != null) {
            return activeTreatment;
        }
        activeTreatment = treatmentRepository.getActiveTreatment(accessToken);
        return activeTreatment;
    }
}
