package com.example.healthcoins.pages.wallet;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.WalletHomeFragmentBinding;
import com.example.healthcoins.services.TokenService;
import com.example.healthcoins.utils.MathUtils;

import java.util.Objects;

import es.dmoral.toasty.Toasty;

public class WalletHome extends Fragment {

    private WalletHomeViewModel walletViewModel;
    private WalletHomeFragmentBinding walletBinding;
    private String accesToken;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        accesToken = TokenService.getInstance(getActivity()).getToken().getAccessToken();
        walletViewModel = ViewModelProviders.of(this).get(WalletHomeViewModel.class);
        walletBinding = DataBindingUtil.inflate(inflater, R.layout.wallet_home_fragment, container, false);
        setInitalBindings();
        loadWallet();
        loadActiveTreatment();
        return walletBinding.getRoot();
    }

    private void setInitalBindings() {
        walletBinding.setIsLoadingBalance(true);
        walletBinding.setIsLoadingTreatment(true);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private void loadWallet() {
        walletViewModel.getBalance(accesToken).observe(this, balance -> {
            if(balance.data != null) {
                double balanceValue = MathUtils.round(balance.data.getBalance(), 2);
                walletBinding.setBalanceValue(balanceValue);
            } else {
                walletBinding.setBalanceValue(0.0);
                Toasty.error(getActivity(), balance.message, Toast.LENGTH_LONG).show();
            }
            walletBinding.setIsLoadingBalance(false);
        });
    }

    private void loadActiveTreatment() {
        walletViewModel.getActiveTreatment(accesToken).observe(this, treatmentDetailResource -> {
            if(treatmentDetailResource.data != null) {
                walletBinding.setActiveTreatment(treatmentDetailResource.data);
            } else {
                Toasty.error(getActivity(), treatmentDetailResource.message, Toast.LENGTH_LONG).show();
            }
            walletBinding.setIsLoadingTreatment(false);
        });
    }
}

