package com.example.healthcoins.pages.home.careprovider;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.view.View;

import com.android.databinding.library.baseAdapters.BR;
import com.example.healthcoins.domain.treatment.TreatmentRecord;

public class CareProviderHomeBindingModel extends BaseObservable {

    private boolean loadingPendingTreatments = true;
    private boolean loadingStartedTreatments = true;
    private boolean loadingSignedTreatments = true;

    @Bindable
    public boolean getLoadingPendingTreatments() {
        return this.loadingPendingTreatments;
    }

    public void setLoadingPendingTreatments(boolean value) {
        if(this.loadingPendingTreatments != value) {
            this.loadingPendingTreatments = value;
            notifyPropertyChanged(BR.loadingPendingTreatments);
        }
    }

    @Bindable
    public boolean getLoadingStartedTreatments() {
        return this.loadingStartedTreatments;
    }

    public void setLoadingStartedTreatments(boolean value) {
        if(this.loadingStartedTreatments != value) {
            this.loadingStartedTreatments = value;
            notifyPropertyChanged(BR.loadingStartedTreatments);
        }
    }

    @Bindable
    public boolean getLoadingSignedTreatments() {
        return this.loadingSignedTreatments;
    }

    public void setLoadingSignedTreatments(boolean visible) {
        if(this.loadingSignedTreatments != visible) {
            this.loadingSignedTreatments = visible;
            notifyPropertyChanged(BR.loadingSignedTreatments);
        }
    }

    @BindingAdapter("android:visibility")
    public static void setVisibility(View view, Boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    public boolean getListIsEmpty(TreatmentRecord[] list) {
        return list != null && list.length == 0;
    }
}
