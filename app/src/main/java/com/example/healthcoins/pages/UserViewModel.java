package com.example.healthcoins.pages;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.healthcoins.domain.Resource;
import com.example.healthcoins.domain.user.careprofessional.CareProfessional;
import com.example.healthcoins.domain.user.participant.Participant;
import com.example.healthcoins.repository.UserRepository;

public class UserViewModel extends ViewModel {
    private LiveData<Resource<CareProfessional>> careProfessionalData;
    private LiveData<Resource<Participant>> participantData;
    private UserRepository userRepo = new UserRepository();

    public LiveData<Resource<CareProfessional>> getCareProfessionalData(String careProfessionalSenderAddress, String bearerToken) {
        if (careProfessionalData != null) {
            return careProfessionalData;
        }
        careProfessionalData = userRepo.getCareProfessionalByAddress(careProfessionalSenderAddress, bearerToken);
        return careProfessionalData;
    }

    public LiveData<Resource<Participant>> getParticipantData(String participantAddress, String bearerToken) {
        if (participantData != null) {
            return participantData;
        }
        participantData = userRepo.getParticipantByAddress(participantAddress, bearerToken);
        return participantData;
    }
}
