package com.example.healthcoins.pages.treatments.start;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.widget.EditText;

import com.example.healthcoins.BR;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.domain.user.participant.Participant;

public class StartTreatmentBindingModel extends BaseObservable {
    private boolean treatmentIsLoading = false;
    private boolean participantIsLoading = false;
    private TreatmentDetail treatmentDetail;
    private Participant participant;

    @Bindable
    public boolean getTreatmentIsLoading() {
        return treatmentIsLoading;
    }

    public void setTreatmentIsLoading(boolean treatmentIsLoading) {
        this.treatmentIsLoading = treatmentIsLoading;
        notifyPropertyChanged(BR.treatmentIsLoading);

    }

    @Bindable
    public TreatmentDetail getTreatmentDetail() {
        return treatmentDetail;
    }

    public void setTreatmentDetail(TreatmentDetail treatmentDetail) {
        this.treatmentDetail = treatmentDetail;
    }

    @Bindable
    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
        notifyPropertyChanged(BR.participant);
    }

    @Bindable
    public boolean getIsParticipantIsLoading() {
        return participantIsLoading;
    }

    public void setParticipantIsLoading(boolean participantIsLoading) {
        this.participantIsLoading = participantIsLoading;
        notifyPropertyChanged(BR.isLoadingParticipant);
    }
}
