package com.example.healthcoins.pages.treatments.stepregister;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.healthcoins.domain.treatment.measurement.MeasurementGoalValue;
import com.example.healthcoins.R;

import java.util.List;

public class GoalTreatmentAdapter extends ArrayAdapter<MeasurementGoalValue> {

    AlertDialog.Builder alert;
    Context context;
    int layoutResourceId;

    private static final String GOALTREATMENT_ADAPTER = "GoalTreatmentAdapter";

    List<MeasurementGoalValue> data;

    public GoalTreatmentAdapter(Context context, int layoutResourceId, List<MeasurementGoalValue> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
        alert = new AlertDialog.Builder(context);
        alert.setTitle("Hulp");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MeasurementValueHolder holder;

        if(row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new MeasurementValueHolder();
            holder.text = row.findViewById(R.id.tr_goalvalue_text);
            holder.help = (ImageButton) row.findViewById(R.id.tr_goalvalue_help);
            row.setTag(holder);
        } else {
            holder = (MeasurementValueHolder) row.getTag();
        }

        MeasurementGoalValue value = data.get(position);
        Log.d(this.GOALTREATMENT_ADAPTER, value.toString());
        if(value.isHelpUI()) { // if help should be visible.
            holder.help.setVisibility(View.VISIBLE);
            row.setOnClickListener(click -> {
                alert.setTitle(value.getHelpText());
                alert.show();
            });
        }
        holder.text.setText(value.toString());
        return row;
    }

    static class MeasurementValueHolder {
        TextView text;
        ImageView help;
    }
}
