package com.example.healthcoins.pages.treatments.stepregister;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.TreatmentHomeViewFragmentBinding;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.domain.treatment.measurement.Measurement;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;
import com.example.healthcoins.pages.treatments.TreatmentBaseViewModel;
import com.example.healthcoins.pages.treatments.TreatmentListViewAdapter;
import com.example.healthcoins.pages.treatments.TreatmentListViewValue;
import com.example.healthcoins.pages.treatments.GoalListAdapter;
import com.example.healthcoins.utils.DateUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class TreatmentHome extends Fragment {

    private TreatmentDetail treatmentDetail;
    private TreatmentBaseViewModel treatmentBaseViewModel;
    private TreatmentHomeViewFragmentBinding binding;
    private TreatmentHomeViewModel treatmentHomeViewModel;
    private NavController navController;
    List<Measurement> currentMeasurements;

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        initializeViewModel();
        binding = DataBindingUtil.inflate(inflater, R.layout.treatment_home_fragment, container, false);
        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding.setHasError(false);
        binding.setIsLoading(true);

        treatmentBaseViewModel.getSelectedTreatmentDetail().observe(getActivity(), treatmentResponse -> {
            treatmentDetail = treatmentResponse;
            binding.setTreatmentDetail(treatmentDetail);
            binding.setHasError(true);
            loadTreatmentPlanData();
            binding.setIsLoading(false);
        });

        loadAddMeasurementsButton();
        loadViewCareProfessionalButton();
        loadMeasurementsButton();
    }

    private void loadTreatmentPlanData() {
        if (treatmentDetail == null) {
            return;
        }
        setTreatmentTitleText("behandelplan " + treatmentDetail.getTreatmentData().getId());
        setTreatmentPlanValues();

        boolean hasStartMeasurement = treatmentDetail.hasStartMeasurement();
        binding.setHasStartMeasurement(hasStartMeasurement);
        currentMeasurements =  new ArrayList<>(Arrays.asList(treatmentDetail.getTreatmentData().getCustomMeasurements()));
        treatmentHomeViewModel.setSelectMeasurements(currentMeasurements);
        if (hasStartMeasurement) {
            setStartMeasurementValues();
        }
        setLatestMeasurementValues();
    }

    private void loadMeasurementsButton(){
        binding.viewMeasurementsBtn.setOnClickListener((View view) -> {
            Bundle bundle = new Bundle();
            navController.navigate(R.id.measurementsHome, bundle);
        });
    }

    private void loadAddMeasurementsButton() {
        binding.addMeasurementBtn.setOnClickListener((View view) -> {
            if (notAllowedToAddMeasurements()) {
                Toasty.error(getActivity(), "Het behandelplan is nog niet gestart.", Toast.LENGTH_LONG).show();
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putString("treatmentId", treatmentDetail.getTreatmentData().getId());
            navController.navigate(R.id.addMeasurement, bundle);
        });
    }

    private void loadViewCareProfessionalButton() {
        binding.checkCareProfessionalBtn.setOnClickListener((View view) -> {
            if (notAllowedToViewCareProfessional()) {
                Toasty.error(getActivity(), "Geen zorgprofessional gevonden", Toast.LENGTH_LONG).show();
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putString("careProfessionalAddress", treatmentDetail.getContractData().getCareTaker());
            navController.navigate(R.id.viewCareProfessional, bundle);
        });
    }

    private boolean notAllowedToViewCareProfessional() {
        return treatmentDetail == null || treatmentDetail.getContractData().getCareTaker() == null;
    }

    private boolean notAllowedToAddMeasurements() {
        return treatmentDetail == null || !treatmentDetail.getContractData().isStarted();
    }

    private void setTreatmentTitleText(String text) {
        TextView treatmentTitleTextView = binding.tmJoinedTreatmentNumber;
        treatmentTitleTextView.setText(Html.fromHtml("<font color='#ef4c42'>" + text + "</font>!"));
    }

    private void setTreatmentPlanValues() {
        MeasurementValue[] goalMeasurement = treatmentDetail.getGoalMeasurement();
        setLowerThanComparisonText(goalMeasurement);

        TreatmentListViewAdapter listViewAdapter = new GoalListAdapter(getActivity(), Arrays.asList(goalMeasurement));
        Long startTime = treatmentDetail.getContractData().getStartDate();
        Date startDate = new Date(startTime);
        listViewAdapter.addValue(new TreatmentListViewValue("Startdatum", ":", DateUtils.formatDateOnYear(startDate)));
        binding.trGoalMeasurement.setAdapter(listViewAdapter);
    }

    private void setLowerThanComparisonText(MeasurementValue[] measurementValues) {
        for (MeasurementValue measurementValue : measurementValues) {
            measurementValue.setComparisonText("<");
        }
    }

    private void setStartMeasurementValues() {
        binding.trStartMeasurementList.setAdapter(new GoalListAdapter(getActivity(), Arrays.asList(treatmentDetail.getStartMeasurement())));
    }

    private void setLatestMeasurementValues() {
        ListView latestMeasurementsView = binding.lastMeasurementList;
        Measurement latestMeasurement = getLatestMeasurement();
        if (!latestMeasurement.hasMeasurements()) {
            binding.setHasLatestMeasurement(false);
            return;
        }

        TreatmentListViewAdapter listViewAdapter = new GoalListAdapter(getActivity(), latestMeasurement.getValues());

        listViewAdapter.addValue(new TreatmentListViewValue("Datum", "=", DateUtils.formatDateOnYear(latestMeasurement.getDate())));
        String doneByProfessional = latestMeasurement.isDoneByProfessional() ? "Ja" : "Nee";
        listViewAdapter.addValue(new TreatmentListViewValue("Door specialist ", "=", doneByProfessional));
        latestMeasurementsView.setAdapter(listViewAdapter);
        loadAddMeasurementsButton();
    }

    private Measurement getLatestMeasurement() {
        if (treatmentDetail.hasEndMeasurement()) {
            return createEndMeasurement();
        }
        if (treatmentDetail.hasCustomMeasurement()) {
            return currentMeasurements.get(currentMeasurements.size() - 1);
        }
        return createStartMeasurement();
    }

    private Measurement createEndMeasurement() {
        Date endDate = new Date(treatmentDetail.getContractData().getPlannedEndDate());
        Measurement endMeasurement = new Measurement();
        endMeasurement.setDate(endDate);
        endMeasurement.setDoneByProfessional(true);
        for (MeasurementValue value : treatmentDetail.getEndMeasurement()) {
            endMeasurement.addValue(value);
        }
        return endMeasurement;
    }

    private Measurement createStartMeasurement() {
        Date startDate = new Date(treatmentDetail.getContractData().getStartDate());
        Measurement startMeasurement = new Measurement();
        startMeasurement.setDate(startDate);
        startMeasurement.setDoneByProfessional(true);
        for (MeasurementValue value : treatmentDetail.getStartMeasurement()) {
            startMeasurement.addValue(value);
        }
        return startMeasurement;
    }

    private void initializeViewModel() {
        treatmentBaseViewModel = ViewModelProviders.of(getActivity()).get(TreatmentBaseViewModel.class);
        treatmentHomeViewModel = ViewModelProviders.of(getActivity()).get(TreatmentHomeViewModel.class);
    }
}
