package com.example.healthcoins.pages.treatments.newtreatment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import com.example.healthcoins.R;
import com.example.healthcoins.databinding.NewTreatmentParticipantSearchRowBinding;
import com.example.healthcoins.domain.user.UserData;
import com.example.healthcoins.domain.user.participant.Participant;

import java.util.ArrayList;
import java.util.List;

public class ParticipantListAdapter extends BaseAdapter implements Filterable {
    private List<Participant> userDataUnfiltered;
    private List<Participant> userDataFiltered;
    private ValueFilter valueFilter;
    private LayoutInflater inflater;
    private static final Integer MAX_AMOUNT_OF_RESULTS_TO_SHOW = 3;

    public ParticipantListAdapter(List<Participant> userDataList) {
        userDataUnfiltered = userDataList;
        userDataFiltered = userDataList;
    }

    @Override
    public int getCount() {
        if(userDataUnfiltered != null) {
            return userDataUnfiltered.size();
        }
        return 0;
    }

    @Override
    public Participant getItem(int position) {
        return userDataUnfiltered.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        if (inflater == null) {
            inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        NewTreatmentParticipantSearchRowBinding rowItemBinding = DataBindingUtil.inflate(inflater, R.layout.new_treatment_participant_search_row, parent, false);
        rowItemBinding.searchResult.setText(getItem(position).getPolicyNumber());

        return rowItemBinding.getRoot();
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (isValidConstraint(constraint)) {
                String upperCaseConstraint = constraint.toString().toUpperCase();
                List<Participant> filterList = getUserDataFiltered(upperCaseConstraint);
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = userDataFiltered.size();
                results.values = userDataFiltered;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            userDataUnfiltered = (List<Participant>) results.values;
            notifyDataSetChanged();
        }
    }

    private List<Participant> getUserDataFiltered(String constraint) {
        List<Participant> filterList = new ArrayList<>();
        Integer maxAmount = MAX_AMOUNT_OF_RESULTS_TO_SHOW > getCount() ? getCount() : MAX_AMOUNT_OF_RESULTS_TO_SHOW;
        for (int i = 0; i < maxAmount; i++) {
            if ((userDataFiltered.get(i).getId().toUpperCase()).contains(constraint)) {
                filterList.add(getItem(i));
            }
        }
        return filterList;
    }

    private boolean isValidConstraint(CharSequence constraint) {
        return constraint != null && constraint.length() > 0;
    }
}
