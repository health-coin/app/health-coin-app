package com.example.healthcoins.pages.treatments.newtreatment.modifygoal;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.ModifyGoalMeasurementFragmentBinding;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;
import com.example.healthcoins.pages.treatments.newtreatment.NewTreatmentViewModel;
import com.example.healthcoins.utils.Constants;

import java.util.List;

import es.dmoral.toasty.Toasty;

public class ModifyGoalMeasurementFragment extends Fragment {

    private NavController controller;
    private NewTreatmentViewModel viewModel;
    private ModifyGoalMeasurementFragmentBinding binding;

    public static ModifyGoalMeasurementFragment newInstance() {
        return new ModifyGoalMeasurementFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this.getActivity()).get(NewTreatmentViewModel.class);
        viewModel.getSelectedMeasurement().observe(this, measurementValue -> {
            binding.setMeasurementValue(measurementValue);
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding  =  DataBindingUtil.inflate(
                inflater, R.layout.modify_goal_measurement_fragment, container, false);

        this.controller = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

        binding.setCallback(this);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void onSaveMeasurementGoalClicked(View view) {
        Toasty.success(getActivity(), "Doel succesvol aangepast", Toast.LENGTH_SHORT).show();
        this.controller.popBackStack();
    }

}
