package com.example.healthcoins.pages.auth.register;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.healthcoins.R;

class CustomSpinner {

    private Spinner spinner;
    private ArrayAdapter<CharSequence> adapter;

    public CustomSpinner(Spinner spinner, Context context) {
        this.spinner = spinner;
        adapter = ArrayAdapter.createFromResource(
                context, R.array.gender_array, android.R.layout.simple_spinner_item
        );
    }

    void loadSpinner() {
        setSpinnerAdapter();
        setSpinnerAction();
    }

    private ArrayAdapter<CharSequence> setSpinnerAdapter() {
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        return adapter;
    }

    private void setSpinnerAction() {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setSpinnerSelection(adapter, position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                setSpinnerSelection(adapter, 0);
            }
        });
    }

    private void setSpinnerSelection(ArrayAdapter<CharSequence> adapter, int position) {
        spinner.setSelection(position);
        adapter.notifyDataSetChanged();
    }
}
