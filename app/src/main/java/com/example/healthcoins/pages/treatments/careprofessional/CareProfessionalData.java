package com.example.healthcoins.pages.treatments.careprofessional;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.CareProfessionalDataFragmentBinding;
import com.example.healthcoins.domain.user.careprofessional.CareProfessional;
import com.example.healthcoins.pages.UserViewModel;
import com.example.healthcoins.services.TokenService;

public class CareProfessionalData extends Fragment {

    private final String CARE_PROFESSIONAL_PROPERTY = "careProfessionalAddress";

    private String careProfessionalAddress;
    private UserViewModel userViewModel;
    private CareProfessionalDataFragmentBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.care_professional_data_fragment, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (isMissingCareProfessionalAddress()) {
            binding.setHasError(true);
            return;
        }
        careProfessionalAddress = getArguments().getString(CARE_PROFESSIONAL_PROPERTY);
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        binding.setHasError(false);
        binding.setIsLoading(true);
        loadCareProfessional();
    }

    private boolean isMissingCareProfessionalAddress() {
        return getArguments() == null || getArguments().get(CARE_PROFESSIONAL_PROPERTY) == null;
    }

    private void loadCareProfessional() {
        String accessToken = TokenService.getInstance(getActivity()).getToken().getAccessToken();

        userViewModel.getCareProfessionalData(careProfessionalAddress, accessToken).observe(getActivity(), response -> {
            if (response.data == null) {
                binding.setHasError(true);
            } else {
                CareProfessional careProfessional = response.data;
                binding.setCareProfessional(careProfessional);
                binding.setLocation(careProfessional.getOrganisation().getFirstLocation());
            }
            binding.setIsLoading(false);
        });
    }
}
