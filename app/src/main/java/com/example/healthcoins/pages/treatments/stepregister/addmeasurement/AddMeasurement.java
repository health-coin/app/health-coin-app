package com.example.healthcoins.pages.treatments.stepregister.addmeasurement;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.AddMeasurementFragmentBinding;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;
import com.example.healthcoins.services.TokenService;
import com.example.healthcoins.validators.NumberViewValidator;
import com.example.healthcoins.validators.WidgetValidator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class AddMeasurement extends Fragment {
    private static final String TREATMENT_ID_PROPERTY = "treatmentId";

    private String accessToken;
    private String treatmentId;
    private AddMeasurementViewModel measurementViewModel;
    private AddMeasurementFragmentBinding binding;
    private List<MeasurementValue> goalValues;
    private NavController navController;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

        binding = DataBindingUtil.inflate(inflater, R.layout.add_measurement_fragment, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        measurementViewModel = ViewModelProviders.of(this).get(AddMeasurementViewModel.class);

        if (isMissingTreatmentId()) {
            binding.setHasError(true);
            return;
        }
        treatmentId = getArguments().getString(TREATMENT_ID_PROPERTY);
        accessToken = TokenService.getInstance(getActivity()).getToken().getAccessToken();

        binding.setHasError(false);
        binding.setIsLoading(true);
        binding.setCallback(this);
        loadTreatmentPlan();
    }

    public void saveMeasurements(View view) {
        if (goalValues == null || goalValues.size() <= 0) {
            showNoMeasurementsFoundError();
            return;
        }
        WidgetValidator<TextView> quantityValidator = new NumberViewValidator(
                numberView -> Toasty.error(getActivity(), numberView.getText().toString() + " is ongeldig.", Toast.LENGTH_LONG).show()
        );

        List<MeasurementValue> newMeasurements = new ArrayList<>();
        for (int i = 0; i < goalValues.size(); i++) {
            View listView = binding.measurementsList.getChildAt(i);
            TextView unitText = listView.findViewById(R.id.measurement_unit);
            EditText quantityText = listView.findViewById(R.id.measurement_quantity);

            boolean isValidMeasurement = quantityValidator.validate(quantityText);
            if (!isValidMeasurement) {
                return;
            }

            MeasurementValue customMeasurement = new MeasurementValue("id", unitText.getText().toString(), Integer.parseInt(quantityText.getText().toString()));
            newMeasurements.add(customMeasurement);
        }
        if (newMeasurements.isEmpty()) {
            showNoMeasurementsFoundError();
            return;
        }

        MeasurementValue[] customMeasurements = newMeasurements.toArray(new MeasurementValue[0]);
        measurementViewModel.createTreatmentMeasurement(customMeasurements, treatmentId, accessToken).observe(getActivity(), response -> {
            if (hasAndShowError(response.code, response.message)) {
                return;
            }
            navController.popBackStack();
            Toasty.success(getActivity(), "Metingen opgeslagen", Toast.LENGTH_LONG).show();
        });
    }

    private boolean hasAndShowError(String code, String message) {
        boolean hasError = code != null;
        if(code != null) {
            Toasty.error(getActivity(), message, Toast.LENGTH_LONG).show();
        } else {
            Toasty.success(getActivity(), message, Toast.LENGTH_LONG).show();
        }
        return hasError;
    }

    private void showNoMeasurementsFoundError() {
        Toasty.error(getActivity(), "Geen metingen gevonden om op te slaan.", Toast.LENGTH_LONG).show();
    }

    private void loadTreatmentPlan() {
        measurementViewModel.getTreatmentPlanById(treatmentId, accessToken).observe(getActivity(), treatmentResponse -> {
            if (isMissingGoalMeasurement(treatmentResponse.data)) {
                binding.setHasError(true);
            } else {
                goalValues = Arrays.asList(treatmentResponse.data.getGoalMeasurement());
                loadGoalValuesList();
            }
            binding.setIsLoading(false);
        });
    }

    private void loadGoalValuesList() {
        List<MeasurementValue> cloneGoalValues = new ArrayList<>(goalValues);
        AddCustomMeasurementListAdapter customMeasurementListAdapter = new AddCustomMeasurementListAdapter(getActivity(), cloneGoalValues);
        binding.measurementsList.setAdapter(customMeasurementListAdapter);
    }

    private boolean isMissingGoalMeasurement(TreatmentDetail treatmentDetail) {
        return treatmentDetail == null || treatmentDetail.getGoalMeasurement() == null;
    }

    private boolean isMissingTreatmentId() {
        return getArguments() == null || getArguments().getString(TREATMENT_ID_PROPERTY) == null;
    }
}

