package com.example.healthcoins.pages.treatments.stepdone;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.healthcoins.R;
import com.example.healthcoins.databinding.TreatmentDoneFragmentBinding;
import com.example.healthcoins.domain.treatment.Treatment;
import com.example.healthcoins.domain.treatment.TreatmentDetail;
import com.example.healthcoins.domain.treatment.measurement.Measurement;
import com.example.healthcoins.domain.treatment.measurement.MeasurementValue;
import com.example.healthcoins.pages.treatments.GoalListAdapter;
import com.example.healthcoins.pages.treatments.TreatmentBaseViewModel;
import com.example.healthcoins.pages.treatments.TreatmentListViewAdapter;
import com.example.healthcoins.pages.treatments.TreatmentListViewValue;
import java.util.Arrays;
import java.util.List;

public class TreatmentDone extends Fragment {

    private TreatmentDoneFragmentBinding viewBinding;
    private TreatmentBaseViewModel baseViewModel;

    public static TreatmentDone newInstance() {
        return new TreatmentDone();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        viewBinding = DataBindingUtil.inflate(inflater, R.layout.treatment_done_fragment, container, false);
        loadTreatmentData();
        return viewBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @SuppressLint("SetTextI18n")
    private void setFinalResultResponse() {
        TextView finalWrapUp = viewBinding.finalWrapupText;
        Treatment treatment = viewBinding.getTreatmentDetail().getTreatmentData();
        finalWrapUp.setText("Je hebt behandelplan "
                + treatment.getId() + " succesvol afgerond en "
                + viewBinding.getTreatmentDetail().getFinalCoins()+ " coins verdiend!");
    }

    private void loadTreatmentData() {
        baseViewModel = ViewModelProviders.of(getActivity()).get(TreatmentBaseViewModel.class);
        baseViewModel.getSelectedTreatmentDetail().observe(getActivity(), this::handleTreatmentDetailResponse);

    }

    private void handleTreatmentDetailResponse(TreatmentDetail treatmentDetail) {
        viewBinding.setTreatmentDetail(treatmentDetail);
        viewBinding.setIsLoadingTreatmentDetail(false);
        setBehandelplanList();
        setFinalResultResponse();
        setFinalResults();
    }

    private void setFinalResults() {
        ListView latestMeasurementsView = viewBinding.archievedResultList;
        List<MeasurementValue> latestMeasurement = Arrays.asList(viewBinding.getTreatmentDetail().getEndMeasurement());
        TreatmentListViewAdapter listViewAdapter = new GoalListAdapter(getActivity(), latestMeasurement);
        latestMeasurementsView.setAdapter(listViewAdapter);
    }


    private void setBehandelplanList() {
        ListView behandelPlanList = viewBinding.behandelplanList;
        List<MeasurementValue> measurementValueList = Arrays.asList(viewBinding.getTreatmentDetail().getGoalMeasurement());
        TreatmentListViewAdapter listAdapter = new GoalListAdapter(getActivity(), measurementValueList);
        behandelPlanList.setAdapter(listAdapter);
    }

}
