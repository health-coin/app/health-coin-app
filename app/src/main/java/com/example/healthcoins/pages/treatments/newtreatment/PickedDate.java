package com.example.healthcoins.pages.treatments.newtreatment;

import android.databinding.ObservableInt;
import android.widget.DatePicker;

import com.example.healthcoins.domain.treatment.CreateTreatmentRequest;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class PickedDate {
    public final ObservableInt year = new ObservableInt();
    public final ObservableInt month = new ObservableInt();
    public final ObservableInt day = new ObservableInt();
    private CreateTreatmentRequest model;

    public PickedDate(CreateTreatmentRequest model) {
        Calendar calendar = Calendar.getInstance();
        year.set(calendar.get(Calendar.YEAR));
        month.set(calendar.get(Calendar.MONTH));
        day.set(calendar.get(Calendar.DAY_OF_MONTH));
        this.model = model;
    }

    public void dateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        this.year.set(year);
        this.month.set(monthOfYear);
        this.day.set(dayOfMonth);
        this.model.setEndDate(this.toString());
    }

    /**
     * Format which is supported by Healthcoin API
     */
    @Override
    public String toString() {
        return this.year.get() + "-" + (this.month.get() +1) + "-" + this.day.get();
    }
}