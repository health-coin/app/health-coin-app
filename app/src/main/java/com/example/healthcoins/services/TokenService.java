package com.example.healthcoins.services;

import android.content.Context;

import com.example.healthcoins.domain.auth.TokenData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TokenService extends StorageService {

    private static final String ACCESS_TOKEN_KEY = "accessToken";
    private static final String EXPIRATION_DATE_KEY = "expirationDate";
    private static final String SECURITY_PREFS_KEY = "securityPrefs";

    private static TokenService instance;

    private TokenService(Context context) {
        super(context, SECURITY_PREFS_KEY);
    }

    public void setToken(TokenData token) {
        this.setStorageString(ACCESS_TOKEN_KEY, token.getAccessToken());
        this.setStorageString(EXPIRATION_DATE_KEY, token.getExpirationDate());
    }

    public TokenData getToken() {
        TokenData data = new TokenData(getStorageString(ACCESS_TOKEN_KEY), "", getStorageString(EXPIRATION_DATE_KEY));
        return data;
    }

    public static TokenService getInstance(Context context) {
        if (instance == null) {
            instance = new TokenService(context);
        }
        return instance;
    }

    public boolean isExpired() {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyy-MM-dd'T'hh:mm");

            Date tokenDate = format.parse(getToken().getExpirationDate());
            Date tokenExpirationDate = addHours(tokenDate, 2);
            Calendar now = Calendar.getInstance();
            return now.after(tokenExpirationDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return true;
    }

    private Date addHours(Date date, int hours) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY, hours);
        return calendar.getTime();
    }
}
