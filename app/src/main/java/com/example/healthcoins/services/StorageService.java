package com.example.healthcoins.services;

import android.content.Context;
import android.content.SharedPreferences;

public abstract class StorageService {
    protected String storagePreferences;
    protected Context context;
    protected SharedPreferences.Editor preferenceEditor;

    StorageService(Context context, String storagePrefs) {
        this.context = context;
        this.storagePreferences = storagePrefs;
        preferenceEditor = this.context.getSharedPreferences(this.storagePreferences, Context.MODE_PRIVATE).edit();

    }
    protected String getStorageString(String key) {
        SharedPreferences prefs = this.context.getSharedPreferences(this.storagePreferences, Context.MODE_PRIVATE);
        return prefs.getString(key, "");
    }

    protected void setStorageString(String key, String value) {
        this.preferenceEditor.putString(key, value);
        this.preferenceEditor.commit();
    }
}
