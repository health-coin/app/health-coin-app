package com.example.healthcoins.services;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.healthcoins.domain.user.UserData;
import com.google.gson.Gson;

public class UserService extends StorageService {
    private static UserService instance;
    private Gson gson;
    UserService(Context context) {
        super(context, "userPrefs");
        gson = new Gson();
    }

    public void setUser(UserData user) {
        String json = gson.toJson(user);
        this.setStorageString("user", json);
    }

    public UserData getUser() {
        String json = this.getStorageString("user");
        return gson.fromJson(json, UserData.class);
    }

    public static UserService getInstance(Context context) {
        if (instance == null) {
            instance = new UserService(context);
        }
        return instance;
    }


}
